#!/bin/bash

# Start the mac webapp
/opt/mac/mac-web-1.0-SNAPSHOT/bin/mac-web -mem 200 &

# Force a first request, in order to start the actors work
sleep 120
wget localhost:9000


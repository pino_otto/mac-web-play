// Comment to get more information during initialization
logLevel := Level.Warn
//logLevel := Level.Debug

// Work offline
//offline := true

// The Typesafe repository 
resolvers += "Typesafe repository" at "https://repo.typesafe.com/typesafe/releases/"
//resolvers += Resolver.typesafeRepo("releases")

//resolvers += Resolver.typesafeIvyRepo("releases")

// Use the Play sbt plugin for Play projects
//addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.2.1") // original
addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.2.6")
//addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.3.1")
//addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.4.1")
//addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.6.6")

import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

    val appName         = "mac-web"
    val appVersion      = "1.0-SNAPSHOT"

    val appDependencies = Seq(
      // Add your project dependencies here,
      jdbc,
      anorm,
      "com.typesafe.akka" %% "akka-testkit" % "2.2.0" % "test",
//      "org.scalatest" % "scalatest_2.10" % "2.0" % "test",
      "org.scalatest" % "scalatest_2.10" % "1.9.1" % "test",
      "org.apache.commons" % "commons-text" % "1.8",
      "com.dimingo.poliform" % "mac-commons" % "0.0.1-SNAPSHOT",
//      "securesocial" %% "securesocial" % "2.1.2",
      "ws.securesocial" %% "securesocial" % "2.1.4",
//      "securesocial" %% "securesocial" % "master-SNAPSHOT",
      "com.typesafe" %% "play-plugins-util" % "2.2.0",
      "com.typesafe" %% "play-plugins-mailer" % "2.2.0",
      "org.springframework" % "spring-context" % "3.0.5.RELEASE",
      "org.springframework" % "spring-jdbc" % "3.0.5.RELEASE",
      "org.springframework" % "spring-tx" % "3.0.5.RELEASE"
    )


    val main = play.Project(appName, appVersion, appDependencies).settings(
      // Add your own project settings here  
//      resolvers += Resolver.url("SecureSocial Repository", url("http://securesocial.ws/repository/releases/"))(Resolver.ivyStylePatterns)
//      resolvers += Resolver.url("sbt-plugin-snapshots", new URL("http://repo.scala-sbt.org/scalasbt/sbt-plugin-snapshots/"))(Resolver.ivyStylePatterns)
//      resolvers += Resolver.url("sbt-plugin-releases", new URL("http://repo.scala-sbt.org/scalasbt/sbt-plugin-releases/"))(Resolver.ivyStylePatterns)
//      resolvers += Resolver.url("play-easymail (release)", url("http://joscha.github.com/play-easymail/repo/releases/"))(Resolver.ivyStylePatterns),
//      resolvers += Resolver.url("play-easymail (snapshot)", url("http://joscha.github.com/play-easymail/repo/snapshots/"))(Resolver.ivyStylePatterns),
      resolvers += Resolver.sonatypeRepo("releases"),
      resolvers += Resolver.mavenLocal
    )

}

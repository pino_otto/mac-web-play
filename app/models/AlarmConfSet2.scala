package models

case class AlarmConfSet2(
												 slaveId: Byte,
												 alarmDescription_16: String,
												 alarmDescription_17: String,
												 alarmDescription_18: String,
												 alarmDescription_19: String,
												 alarmDescription_20: String,
												 alarmDescription_21: String,
												 alarmDescription_22: String,
												 alarmDescription_23: String,
												 alarmDescription_24: String,
												 alarmDescription_25: String,
												 alarmDescription_26: String,
												 alarmDescription_27: String,
												 alarmDescription_28: String,
												 alarmDescription_29: String,
												 alarmDescription_30: String
											 )
package models

case class DeviceEmail(
												slaveId: Byte,
												name_1: String,
												phone_1: String,
												email_1: String,
												name_2: String,
												phone_2: String,
												email_2: String,
												name_3: String,
												phone_3: String,
												email_3: String,
												name_4: String,
												phone_4: String,
												email_4: String,
												name_5: String,
												phone_5: String,
												email_5: String
											)
package models

case class Email(from: String, to: String, subject: String, text: String)

package models

case class AlarmConf(
											slaveId: Byte,
											alarmId: Byte,
											alarmDescription: String
										)
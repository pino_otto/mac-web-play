package models;

import java.io.Serializable;



public class SingleAlarm implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int id;  // 1-30
	
	private String description;  // to take from database
	
	private String status;  // e.g. "red", "green"
	
	private String imageFilename;  // e.g "red.jpg", "green.jpg"
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getImageFilename() {
		return imageFilename;
	}

	public void setImageFilename(String imageFilename) {
		this.imageFilename = imageFilename;
	}

	@java.lang.Override
	public java.lang.String toString() {
		return "SingleAlarm{" +
				"id=" + id +
				", description='" + description + '\'' +
				", status='" + status + '\'' +
				", imageFilename='" + imageFilename + '\'' +
				'}';
	}
} // end class

package models

case class AlarmPackage(slaveId: Int, alarmList: List[SingleAlarm])
package models

case class DeviceConf(
											 slaveId: Byte,
											 status: String,
											 serialPort: String,
											 ipAddress: String,
											 ipPort: String,
											 slaveLocation: String,
											 slaveDescription: String
										 )
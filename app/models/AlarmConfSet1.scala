package models

case class AlarmConfSet1(
												 slaveId: Byte,
												 alarmDescription_01: String,
												 alarmDescription_02: String,
												 alarmDescription_03: String,
												 alarmDescription_04: String,
												 alarmDescription_05: String,
												 alarmDescription_06: String,
												 alarmDescription_07: String,
												 alarmDescription_08: String,
												 alarmDescription_09: String,
												 alarmDescription_10: String,
												 alarmDescription_11: String,
												 alarmDescription_12: String,
												 alarmDescription_13: String,
												 alarmDescription_14: String,
												 alarmDescription_15: String
											 )
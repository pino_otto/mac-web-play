/*

Copyright (C) 2008 Giovanni Di Mingo

    This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software 
Foundation; either version 2 of the License, or any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place, 
Suite 330, Boston, MA 02111-1307 USA

Author: Giovanni Di Mingo
Email:  giovanni@dimingo.com

*/

/*
 * MacService.java
 *
 * Created on July 20, 2008, 8:34 PM
 *
 */

package services;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import com.dimingo.poliform.mac.commons.dao.UserDao;
import play.Logger;

import com.dimingo.poliform.mac.commons.bean.Alarm;
import com.dimingo.poliform.mac.commons.bean.AlarmDescription;
import com.dimingo.poliform.mac.commons.bean.Device;
import com.dimingo.poliform.mac.commons.bean.Command;
import com.dimingo.poliform.mac.commons.dao.AlarmDao;
import com.dimingo.poliform.mac.commons.dao.CommandDao;
import com.dimingo.poliform.mac.commons.dao.DeviceDao;

import models.SingleAlarm;
import models.DeviceConf;
import models.DeviceEmail;
import models.AlarmConf;
import models.AlarmConfSet1;
import models.AlarmConfSet2;


public class MacServiceImpl implements MacService {
	
	/** --------------------------------------------------------------------------------------------
	 * Spring injection (values and collaborators)
	 */
	
	private AlarmDao alarmDao;
	
	private CommandDao commandDao;
	
	private DeviceDao deviceDao;

	private UserDao userDao;

	
	// Default constructor
	public MacServiceImpl() {
		
	} // end constructor
	

	/**
	 * 
	 */
	@Override
	public List<SingleAlarm> getAlarms(byte slaveId) {
		
		// get the latest alarm signals from the database
		Alarm alarmPacket = alarmDao.findLastAlarmBySlaveId(slaveId);
		
		// define the list for keeping the single alarms
		List<SingleAlarm> singleAlarmList = new ArrayList<SingleAlarm>();
		
		// check for null alarm packet
		if (alarmPacket == null) {
			
			Logger.info("No alarms found inside the DB");
			
			// return empty list of single alarms
			return singleAlarmList;
			
		} // end if
		
		// -----------------------------------------------
		// get the list of alarm descriptions
		List<AlarmDescription> alarmDescriptionList = getAllAlarmDescriptions(slaveId);
		
		// check for null alarm description list
		if (alarmDescriptionList == null || alarmDescriptionList.size() < 30) {
			
			Logger.error("No alarm descriptions or less than 30 descriptions found inside the DB");
			
			// return empty list of single alarms
			return singleAlarmList;
			
		} // end if
		
		// create a single alarm object
		SingleAlarm singleAlarm = new SingleAlarm();
		
		// define the status
		String status = "";
		
		// -----------------------------------------------
		// Set alarm 01
		
		// set the alarm id
		singleAlarm.setId(1);
		
		// calculate the status
		if (alarmPacket.isRed_01()) { 
			status = "red";
		} else if (alarmPacket.isGreen_01()) {
			status = "green";
		} else {
			status = "gray";
		}
		
		// set the alarm description
		singleAlarm.setDescription(alarmDescriptionList.get(0).getAlarmDescription());
		
		// set the alarm status
		singleAlarm.setStatus(status);
		
		// set the alarm image filename
		singleAlarm.setImageFilename(status + ".png");
		
		// add the single alarm to the list
		singleAlarmList.add(singleAlarm);
		
		// -----------------------------------------------
		// Set alarm 02
		
		// create a single alarm object
		singleAlarm = new SingleAlarm();
		
		// set the alarm id
		singleAlarm.setId(2);
		
		// calculate the status
		if (alarmPacket.isRed_02()) { 
			status = "red";
		}
		else if (alarmPacket.isGreen_02()) { 
			status = "green";
		}
		else {
			status = "gray";
		}
		
		// set the alarm description
		singleAlarm.setDescription(alarmDescriptionList.get(1).getAlarmDescription());
		
		// set the alarm status
		singleAlarm.setStatus(status);
		
		// set the alarm image filename
		singleAlarm.setImageFilename(status + ".png");
		
		// add the single alarm to the list
		singleAlarmList.add(singleAlarm);
		
		// -----------------------------------------------
		// Set alarm 03
		
		// create a single alarm object
		singleAlarm = new SingleAlarm();
		
		// set the alarm id
		singleAlarm.setId(3);
		
		// calculate the status
		if (alarmPacket.isRed_03()) { 
			status = "red";
		}
		else if (alarmPacket.isGreen_03()) { 
			status = "green";
		}
		else {
			status = "gray";
		}
		
		// set the alarm description
		singleAlarm.setDescription(alarmDescriptionList.get(2).getAlarmDescription());
		
		// set the alarm status
		singleAlarm.setStatus(status);
		
		// set the alarm image filename
		singleAlarm.setImageFilename(status + ".png");
		
		// add the single alarm to the list
		singleAlarmList.add(singleAlarm);
		
		// -----------------------------------------------
		// Set alarm 04
		
		// create a single alarm object
		singleAlarm = new SingleAlarm();
		
		// set the alarm id
		singleAlarm.setId(4);
		
		// calculate the status
		if (alarmPacket.isRed_04()) { 
			status = "red";
		}
		else if (alarmPacket.isGreen_04()) { 
			status = "green";
		}
		else {
			status = "gray";
		}
		
		// set the alarm description
		singleAlarm.setDescription(alarmDescriptionList.get(3).getAlarmDescription());
		
		// set the alarm status
		singleAlarm.setStatus(status);
		
		// set the alarm image filename
		singleAlarm.setImageFilename(status + ".png");
		
		// add the single alarm to the list
		singleAlarmList.add(singleAlarm);
		
		// -----------------------------------------------
		// Set alarm 05
		
		// create a single alarm object
		singleAlarm = new SingleAlarm();
		
		// set the alarm id
		singleAlarm.setId(5);
		
		// calculate the status
		if (alarmPacket.isRed_05()) { 
			status = "red";
		}
		else if (alarmPacket.isGreen_05()) { 
			status = "green";
		}
		else {
			status = "gray";
		}
		
		// set the alarm description
		singleAlarm.setDescription(alarmDescriptionList.get(4).getAlarmDescription());
		
		// set the alarm status
		singleAlarm.setStatus(status);
		
		// set the alarm image filename
		singleAlarm.setImageFilename(status + ".png");
		
		// add the single alarm to the list
		singleAlarmList.add(singleAlarm);
		
		// -----------------------------------------------
		// Set alarm 06
		
		// create a single alarm object
		singleAlarm = new SingleAlarm();
		
		// set the alarm id
		singleAlarm.setId(6);
		
		// calculate the status
		if (alarmPacket.isRed_06()) { 
			status = "red";
		}
		else if (alarmPacket.isGreen_06()) { 
			status = "green";
		}
		else {
			status = "gray";
		}
		
		// set the alarm description
		singleAlarm.setDescription(alarmDescriptionList.get(5).getAlarmDescription());
		
		// set the alarm status
		singleAlarm.setStatus(status);
		
		// set the alarm image filename
		singleAlarm.setImageFilename(status + ".png");
		
		// add the single alarm to the list
		singleAlarmList.add(singleAlarm);
		
		// -----------------------------------------------
		// Set alarm 07
		
		// create a single alarm object
		singleAlarm = new SingleAlarm();
		
		// set the alarm id
		singleAlarm.setId(7);
		
		// calculate the status
		if (alarmPacket.isRed_07()) { 
			status = "red";
		}
		else if (alarmPacket.isGreen_07()) { 
			status = "green";
		}
		else {
			status = "gray";
		}
		
		// set the alarm description
		singleAlarm.setDescription(alarmDescriptionList.get(6).getAlarmDescription());
		
		// set the alarm status
		singleAlarm.setStatus(status);
		
		// set the alarm image filename
		singleAlarm.setImageFilename(status + ".png");
		
		// add the single alarm to the list
		singleAlarmList.add(singleAlarm);
		
		// -----------------------------------------------
		// Set alarm 08
		
		// create a single alarm object
		singleAlarm = new SingleAlarm();
		
		// set the alarm id
		singleAlarm.setId(8);
		
		// calculate the status
		if (alarmPacket.isRed_08()) { 
			status = "red";
		}
		else if (alarmPacket.isGreen_08()) { 
			status = "green";
		}
		else {
			status = "gray";
		}
		
		// set the alarm description
		singleAlarm.setDescription(alarmDescriptionList.get(7).getAlarmDescription());
		
		// set the alarm status
		singleAlarm.setStatus(status);
		
		// set the alarm image filename
		singleAlarm.setImageFilename(status + ".png");
		
		// add the single alarm to the list
		singleAlarmList.add(singleAlarm);
		
		// -----------------------------------------------
		// Set alarm 09
		
		// create a single alarm object
		singleAlarm = new SingleAlarm();
		
		// set the alarm id
		singleAlarm.setId(9);
		
		// calculate the status
		if (alarmPacket.isRed_09()) { 
			status = "red";
		}
		else if (alarmPacket.isGreen_09()) { 
			status = "green";
		}
		else {
			status = "gray";
		}
		
		// set the alarm description
		singleAlarm.setDescription(alarmDescriptionList.get(8).getAlarmDescription());
		
		// set the alarm status
		singleAlarm.setStatus(status);
		
		// set the alarm image filename
		singleAlarm.setImageFilename(status + ".png");
		
		// add the single alarm to the list
		singleAlarmList.add(singleAlarm);
		
		// -----------------------------------------------
		// Set alarm 10
		
		// create a single alarm object
		singleAlarm = new SingleAlarm();
		
		// set the alarm id
		singleAlarm.setId(10);
		
		// calculate the status
		if (alarmPacket.isRed_10()) { 
			status = "red";
		}
		else if (alarmPacket.isGreen_10()) { 
			status = "green";
		}
		else {
			status = "gray";
		}
		
		// set the alarm description
		singleAlarm.setDescription(alarmDescriptionList.get(9).getAlarmDescription());
		
		// set the alarm status
		singleAlarm.setStatus(status);
		
		// set the alarm image filename
		singleAlarm.setImageFilename(status + ".png");
		
		// add the single alarm to the list
		singleAlarmList.add(singleAlarm);
		
		// -----------------------------------------------
		// Set alarm 11
		
		// create a single alarm object
		singleAlarm = new SingleAlarm();
		
		// set the alarm id
		singleAlarm.setId(11);
		
		// calculate the status
		if (alarmPacket.isRed_11()) { 
			status = "red";
		}
		else if (alarmPacket.isGreen_11()) { 
			status = "green";
		}
		else {
			status = "gray";
		}
		
		// set the alarm description
		singleAlarm.setDescription(alarmDescriptionList.get(10).getAlarmDescription());
		
		// set the alarm status
		singleAlarm.setStatus(status);
		
		// set the alarm image filename
		singleAlarm.setImageFilename(status + ".png");
		
		// add the single alarm to the list
		singleAlarmList.add(singleAlarm);
		
		// -----------------------------------------------
		// Set alarm 12
		
		// create a single alarm object
		singleAlarm = new SingleAlarm();
		
		// set the alarm id
		singleAlarm.setId(12);
		
		// calculate the status
		if (alarmPacket.isRed_12()) { 
			status = "red";
		}
		else if (alarmPacket.isGreen_12()) { 
			status = "green";
		}
		else {
			status = "gray";
		}
		
		// set the alarm description
		singleAlarm.setDescription(alarmDescriptionList.get(11).getAlarmDescription());
		
		// set the alarm status
		singleAlarm.setStatus(status);
		
		// set the alarm image filename
		singleAlarm.setImageFilename(status + ".png");
		
		// add the single alarm to the list
		singleAlarmList.add(singleAlarm);
		
		// -----------------------------------------------
		// Set alarm 13
		
		// create a single alarm object
		singleAlarm = new SingleAlarm();
		
		// set the alarm id
		singleAlarm.setId(13);
		
		// calculate the status
		if (alarmPacket.isRed_13()) { 
			status = "red";
		}
		else if (alarmPacket.isGreen_13()) { 
			status = "green";
		}
		else {
			status = "gray";
		}
		
		// set the alarm description
		singleAlarm.setDescription(alarmDescriptionList.get(12).getAlarmDescription());
		
		// set the alarm status
		singleAlarm.setStatus(status);
		
		// set the alarm image filename
		singleAlarm.setImageFilename(status + ".png");
		
		// add the single alarm to the list
		singleAlarmList.add(singleAlarm);
		
		// -----------------------------------------------
		// Set alarm 14
		
		// create a single alarm object
		singleAlarm = new SingleAlarm();
		
		// set the alarm id
		singleAlarm.setId(14);
		
		// calculate the status
		if (alarmPacket.isRed_14()) { 
			status = "red";
		}
		else if (alarmPacket.isGreen_14()) { 
			status = "green";
		}
		else {
			status = "gray";
		}
		
		// set the alarm description
		singleAlarm.setDescription(alarmDescriptionList.get(13).getAlarmDescription());
		
		// set the alarm status
		singleAlarm.setStatus(status);
		
		// set the alarm image filename
		singleAlarm.setImageFilename(status + ".png");
		
		// add the single alarm to the list
		singleAlarmList.add(singleAlarm);
		
		// -----------------------------------------------
		// Set alarm 15
		
		// create a single alarm object
		singleAlarm = new SingleAlarm();
		
		// set the alarm id
		singleAlarm.setId(15);
		
		// calculate the status
		if (alarmPacket.isRed_15()) { 
			status = "red";
		}
		else if (alarmPacket.isGreen_15()) { 
			status = "green";
		}
		else {
			status = "gray";
		}
		
		// set the alarm description
		singleAlarm.setDescription(alarmDescriptionList.get(14).getAlarmDescription());
		
		// set the alarm status
		singleAlarm.setStatus(status);
		
		// set the alarm image filename
		singleAlarm.setImageFilename(status + ".png");
		
		// add the single alarm to the list
		singleAlarmList.add(singleAlarm);
		
		// -----------------------------------------------
		// Set alarm 16
		
		// create a single alarm object
		singleAlarm = new SingleAlarm();
		
		// set the alarm id
		singleAlarm.setId(16);
		
		// calculate the status
		if (alarmPacket.isRed_16()) { 
			status = "red";
		}
		else if (alarmPacket.isGreen_16()) { 
			status = "green";
		}
		else {
			status = "gray";
		}
		
		// set the alarm description
		singleAlarm.setDescription(alarmDescriptionList.get(15).getAlarmDescription());
		
		// set the alarm status
		singleAlarm.setStatus(status);
		
		// set the alarm image filename
		singleAlarm.setImageFilename(status + ".png");
		
		// add the single alarm to the list
		singleAlarmList.add(singleAlarm);
		
		// -----------------------------------------------
		// Set alarm 17
		
		// create a single alarm object
		singleAlarm = new SingleAlarm();
		
		// set the alarm id
		singleAlarm.setId(17);
		
		// calculate the status
		if (alarmPacket.isRed_17()) { 
			status = "red";
		}
		else if (alarmPacket.isGreen_17()) { 
			status = "green";
		}
		else {
			status = "gray";
		}
		
		// set the alarm description
		singleAlarm.setDescription(alarmDescriptionList.get(16).getAlarmDescription());
		
		// set the alarm status
		singleAlarm.setStatus(status);
		
		// set the alarm image filename
		singleAlarm.setImageFilename(status + ".png");
		
		// add the single alarm to the list
		singleAlarmList.add(singleAlarm);
		
		// -----------------------------------------------
		// Set alarm 18
		
		// create a single alarm object
		singleAlarm = new SingleAlarm();
		
		// set the alarm id
		singleAlarm.setId(18);
		
		// calculate the status
		if (alarmPacket.isRed_18()) { 
			status = "red";
		}
		else if (alarmPacket.isGreen_18()) { 
			status = "green";
		}
		else {
			status = "gray";
		}
		
		// set the alarm description
		singleAlarm.setDescription(alarmDescriptionList.get(17).getAlarmDescription());
		
		// set the alarm status
		singleAlarm.setStatus(status);
		
		// set the alarm image filename
		singleAlarm.setImageFilename(status + ".png");
		
		// add the single alarm to the list
		singleAlarmList.add(singleAlarm);
		
		// -----------------------------------------------
		// Set alarm 19
		
		// create a single alarm object
		singleAlarm = new SingleAlarm();
		
		// set the alarm id
		singleAlarm.setId(19);
		
		// calculate the status
		if (alarmPacket.isRed_19()) { 
			status = "red";
		}
		else if (alarmPacket.isGreen_19()) { 
			status = "green";
		}
		else {
			status = "gray";
		}
		
		// set the alarm description
		singleAlarm.setDescription(alarmDescriptionList.get(18).getAlarmDescription());
		
		// set the alarm status
		singleAlarm.setStatus(status);
		
		// set the alarm image filename
		singleAlarm.setImageFilename(status + ".png");
		
		// add the single alarm to the list
		singleAlarmList.add(singleAlarm);
		
		// -----------------------------------------------
		// Set alarm 20
		
		// create a single alarm object
		singleAlarm = new SingleAlarm();
		
		// set the alarm id
		singleAlarm.setId(20);
		
		// calculate the status
		if (alarmPacket.isRed_20()) { 
			status = "red";
		}
		else if (alarmPacket.isGreen_20()) { 
			status = "green";
		}
		else {
			status = "gray";
		}
		
		// set the alarm description
		singleAlarm.setDescription(alarmDescriptionList.get(19).getAlarmDescription());
		
		// set the alarm status
		singleAlarm.setStatus(status);
		
		// set the alarm image filename
		singleAlarm.setImageFilename(status + ".png");
		
		// add the single alarm to the list
		singleAlarmList.add(singleAlarm);
		
		// -----------------------------------------------
		// Set alarm 21
		
		// create a single alarm object
		singleAlarm = new SingleAlarm();
		
		// set the alarm id
		singleAlarm.setId(21);
		
		// calculate the status
		if (alarmPacket.isRed_21()) { 
			status = "red";
		}
		else if (alarmPacket.isGreen_21()) { 
			status = "green";
		}
		else {
			status = "gray";
		}
		
		// set the alarm description
		singleAlarm.setDescription(alarmDescriptionList.get(20).getAlarmDescription());
		
		// set the alarm status
		singleAlarm.setStatus(status);
		
		// set the alarm image filename
		singleAlarm.setImageFilename(status + ".png");
		
		// add the single alarm to the list
		singleAlarmList.add(singleAlarm);
		
		// -----------------------------------------------
		// Set alarm 22
		
		// create a single alarm object
		singleAlarm = new SingleAlarm();
		
		// set the alarm id
		singleAlarm.setId(22);
		
		// calculate the status
		if (alarmPacket.isRed_22()) { 
			status = "red";
		}
		else if (alarmPacket.isGreen_22()) { 
			status = "green";
		}
		else {
			status = "gray";
		}
		
		// set the alarm description
		singleAlarm.setDescription(alarmDescriptionList.get(21).getAlarmDescription());
		
		// set the alarm status
		singleAlarm.setStatus(status);
		
		// set the alarm image filename
		singleAlarm.setImageFilename(status + ".png");
		
		// add the single alarm to the list
		singleAlarmList.add(singleAlarm);
		
		// -----------------------------------------------
		// Set alarm 23
		
		// create a single alarm object
		singleAlarm = new SingleAlarm();
		
		// set the alarm id
		singleAlarm.setId(23);
		
		// calculate the status
		if (alarmPacket.isRed_23()) { 
			status = "red";
		}
		else if (alarmPacket.isGreen_23()) { 
			status = "green";
		}
		else {
			status = "gray";
		}
		
		// set the alarm description
		singleAlarm.setDescription(alarmDescriptionList.get(22).getAlarmDescription());
		
		// set the alarm status
		singleAlarm.setStatus(status);
		
		// set the alarm image filename
		singleAlarm.setImageFilename(status + ".png");
		
		// add the single alarm to the list
		singleAlarmList.add(singleAlarm);
		
		// -----------------------------------------------
		// Set alarm 24
		
		// create a single alarm object
		singleAlarm = new SingleAlarm();
		
		// set the alarm id
		singleAlarm.setId(24);
		
		// calculate the status
		if (alarmPacket.isRed_24()) { 
			status = "red";
		}
		else if (alarmPacket.isGreen_24()) { 
			status = "green";
		}
		else {
			status = "gray";
		}
		
		// set the alarm description
		singleAlarm.setDescription(alarmDescriptionList.get(23).getAlarmDescription());
		
		// set the alarm status
		singleAlarm.setStatus(status);
		
		// set the alarm image filename
		singleAlarm.setImageFilename(status + ".png");
		
		// add the single alarm to the list
		singleAlarmList.add(singleAlarm);
		
		// -----------------------------------------------
		// Set alarm 25
		
		// create a single alarm object
		singleAlarm = new SingleAlarm();
		
		// set the alarm id
		singleAlarm.setId(25);
		
		// calculate the status
		if (alarmPacket.isRed_25()) { 
			status = "red";
		}
		else if (alarmPacket.isGreen_25()) { 
			status = "green";
		}
		else {
			status = "gray";
		}
		
		// set the alarm description
		singleAlarm.setDescription(alarmDescriptionList.get(24).getAlarmDescription());
		
		// set the alarm status
		singleAlarm.setStatus(status);
		
		// set the alarm image filename
		singleAlarm.setImageFilename(status + ".png");
		
		// add the single alarm to the list
		singleAlarmList.add(singleAlarm);
		
		// -----------------------------------------------
		// Set alarm 26
		
		// create a single alarm object
		singleAlarm = new SingleAlarm();
		
		// set the alarm id
		singleAlarm.setId(26);
		
		// calculate the status
		if (alarmPacket.isRed_26()) { 
			status = "red";
		}
		else if (alarmPacket.isGreen_26()) { 
			status = "green";
		}
		else {
			status = "gray";
		}
		
		// set the alarm description
		singleAlarm.setDescription(alarmDescriptionList.get(25).getAlarmDescription());
		
		// set the alarm status
		singleAlarm.setStatus(status);
		
		// set the alarm image filename
		singleAlarm.setImageFilename(status + ".png");
		
		// add the single alarm to the list
		singleAlarmList.add(singleAlarm);
		
		// -----------------------------------------------
		// Set alarm 27
		
		// create a single alarm object
		singleAlarm = new SingleAlarm();
		
		// set the alarm id
		singleAlarm.setId(27);
		
		// calculate the status
		if (alarmPacket.isRed_27()) { 
			status = "red";
		}
		else if (alarmPacket.isGreen_27()) { 
			status = "green";
		}
		else {
			status = "gray";
		}
		
		// set the alarm description
		singleAlarm.setDescription(alarmDescriptionList.get(26).getAlarmDescription());
		
		// set the alarm status
		singleAlarm.setStatus(status);
		
		// set the alarm image filename
		singleAlarm.setImageFilename(status + ".png");
		
		// add the single alarm to the list
		singleAlarmList.add(singleAlarm);
		
		// -----------------------------------------------
		// Set alarm 28
		
		// create a single alarm object
		singleAlarm = new SingleAlarm();
		
		// set the alarm id
		singleAlarm.setId(28);
		
		// calculate the status
		if (alarmPacket.isRed_28()) { 
			status = "red";
		}
		else if (alarmPacket.isGreen_28()) { 
			status = "green";
		}
		else {
			status = "gray";
		}
		
		// set the alarm description
		singleAlarm.setDescription(alarmDescriptionList.get(27).getAlarmDescription());
		
		// set the alarm status
		singleAlarm.setStatus(status);
		
		// set the alarm image filename
		singleAlarm.setImageFilename(status + ".png");
		
		// add the single alarm to the list
		singleAlarmList.add(singleAlarm);
		
		// -----------------------------------------------
		// Set alarm 29
		
		// create a single alarm object
		singleAlarm = new SingleAlarm();
		
		// set the alarm id
		singleAlarm.setId(29);
		
		// calculate the status
		if (alarmPacket.isRed_29()) { 
			status = "red";
		}
		else if (alarmPacket.isGreen_29()) { 
			status = "green";
		}
		else {
			status = "gray";
		}
		
		// set the alarm description
		singleAlarm.setDescription(alarmDescriptionList.get(28).getAlarmDescription());
		
		// set the alarm status
		singleAlarm.setStatus(status);
		
		// set the alarm image filename
		singleAlarm.setImageFilename(status + ".png");
		
		// add the single alarm to the list
		singleAlarmList.add(singleAlarm);
		
		// -----------------------------------------------
		// Set alarm 30
		
		// create a single alarm object
		singleAlarm = new SingleAlarm();
		
		// set the alarm id
		singleAlarm.setId(30);
		
		// calculate the status
		if (alarmPacket.isRed_30()) { 
			status = "red";
		}
		else if (alarmPacket.isGreen_30()) { 
			status = "green";
		}
		else {
			status = "gray";
		}
		
		// set the alarm description
		singleAlarm.setDescription(alarmDescriptionList.get(29).getAlarmDescription());
		
		// set the alarm status
		singleAlarm.setStatus(status);
		
		// set the alarm image filename
		singleAlarm.setImageFilename(status + ".png");
		
		// add the single alarm to the list
		singleAlarmList.add(singleAlarm);
		
		// -----------------------------------------------
		// return the list of alarms
		return singleAlarmList;
		
	} // end getAlarms
	
	
	/**
	 * 
	 */
	@Override
	public AlarmDescription getAlarmDescription(byte slaveId, byte alarmId) {
	
		// get the alarm description from the database
		AlarmDescription alarmDescription = alarmDao.findAlarmDescription(slaveId, alarmId);
		
		return alarmDescription;
	
	} // end getAlarmDescription
	
	
	/**
	 * 
	 */
	@Override
	public List<AlarmDescription> getAllAlarmDescriptions(byte slaveId) {
		
		// get the alarm description list from the database
		List<AlarmDescription> alarmDescriptionList = alarmDao.findAllAlarmDescriptions(slaveId);
		
		return alarmDescriptionList;
		
	} // end getAllAlarmDescriptions
	
	
	/**
	 * 
	 */
	@Override
	public Boolean getMute(byte slaveId) {
		
		// get the latest alarm signals from the database
		Alarm alarmPacket = alarmDao.findLastAlarmBySlaveId(slaveId);
		
		// check for null alarm packet
		if (alarmPacket == null) {
			
			Logger.info("No alarms found inside the DB");
			
			// return no mute
			return false;
			
		} // end if
		
		return alarmPacket.isMute();
		
	} // end getMute
	
	
	/**
	 * 
	 */
	@Override
	public List<String> getEmails(byte slaveId) {
		
		List<String> emailList = new ArrayList<String>();
		
		// get the device from the database
		Device device = deviceDao.findDeviceBySlaveId(slaveId);
		
		// check for null device
		if (device == null) {
			
			Logger.info("No device found inside the DB");
			
			// return empty list
			return emailList;
			
		} // end if
		
		// get the emails fields from the device
		emailList.add(device.getEmail1());
		emailList.add(device.getEmail2());
		emailList.add(device.getEmail3());
		emailList.add(device.getEmail4());
		emailList.add(device.getEmail5());
		
		return emailList;
		
	} // end getEmails
	
	
	/**
	 * 
	 */
	@Override
	public void resetAlarms(byte slaveId) {
		
		//TODO check if needed to set only one field (reset), without resetting the other
		
		// create a command for reset
		Command resetCommand = new Command();
		resetCommand.setSlaveId(slaveId);
		resetCommand.setReset(true);
		resetCommand.setMute(false);
		
		// write the reset command to the DB
		commandDao.updateCommand(resetCommand);
		
	} // end resetAlarms
	
	
	/**
	 * 
	 */
	@Override
	public void muteAlarms(byte slaveId) {
		
		//TODO check if needed to set only one field (mute), without resetting the other
		
		// create a command for mute
		Command muteCommand = new Command();
		muteCommand.setSlaveId(slaveId);
		muteCommand.setReset(false);
		muteCommand.setMute(true);
		
		// write the mute command to the DB
		commandDao.updateCommand(muteCommand);
		
	} // end muteAlarms


	/**
	 *
	 */
	@Override
	public Boolean getMuteCommand(byte slaveId) {

		Command command = commandDao.findCommandBySlaveId(slaveId);

		return command.isMute();

	} // end getMuteCommand


	/**
	 *
	 */
	@Override
	public void updateMute(byte slaveId, Boolean mute) {

		commandDao.updateMute(slaveId, mute);

	} // end updateMute


	/**
	 *
	 */
	@Override
	public void saveDeviceConf(DeviceConf deviceConf) {

		Device device = new Device();

		device.setSlaveId(deviceConf.slaveId());
		device.setStatus(deviceConf.status());
		device.setSerialPort(deviceConf.serialPort());
		device.setIpAddress(deviceConf.ipAddress());
		device.setIpPort(deviceConf.ipPort());
		device.setSlaveLocation(deviceConf.slaveLocation());
		device.setSlaveDescription(deviceConf.slaveDescription());

		Logger.info("Going to save the device config to the DB");

		deviceDao.updateDeviceConf(device);

		Logger.info("Device config saved to the DB");

	}


	/**
	 *
	 */
	@Override
	public void saveDeviceEmail(DeviceEmail deviceEmail) {

		Device device = new Device();

		device.setSlaveId(deviceEmail.slaveId());
		device.setName1(deviceEmail.name_1());
		device.setPhone1(deviceEmail.phone_1());
		device.setEmail1(deviceEmail.email_1());
		device.setName2(deviceEmail.name_2());
		device.setPhone2(deviceEmail.phone_2());
		device.setEmail2(deviceEmail.email_2());
		device.setName3(deviceEmail.name_3());
		device.setPhone3(deviceEmail.phone_3());
		device.setEmail3(deviceEmail.email_3());
		device.setName4(deviceEmail.name_4());
		device.setPhone4(deviceEmail.phone_4());
		device.setEmail4(deviceEmail.email_4());
		device.setName5(deviceEmail.name_5());
		device.setPhone5(deviceEmail.phone_5());
		device.setEmail5(deviceEmail.email_5());

		Logger.info("Going to save the device email to the DB");

		deviceDao.updateDeviceEmail(device);

		Logger.info("Device email saved to the DB");

	}


	/**
	 *
	 */
	@Override
	public void saveAlarmConf(AlarmConf alarmConf) {

		AlarmDescription alarmDescription = new AlarmDescription();

		alarmDescription.setSlaveId(alarmConf.slaveId());
		alarmDescription.setAlarmId(alarmConf.alarmId());
		alarmDescription.setAlarmDescription(alarmConf.alarmDescription());

		Logger.info("Going to save the alarm decsription to the DB");

		alarmDao.updateAlarmDescription(alarmDescription);

		Logger.info("Alarm description saved to the DB");

	}


	/**
	 *
	 */
	@Override
	public void saveSmtpConf(String smtpConf) {

		Logger.info("Going to save the smtp configuration to smtp.conf file");

		Logger.debug("smtpConf = " + smtpConf);

		try {
			// write smtp.conf to disk
			Files.write(Paths.get("/opt/mac/smtp.conf"), smtpConf.getBytes());
			Logger.info("Smtp configuration saved to smtp.conf file");
		} catch (IOException e) {
			e.printStackTrace();
			Logger.error("Error while writing smtp.conf file");
		}

	}


	/**
	 *
	 */
	@Override
	public DeviceConf getDeviceConf(byte slaveId) {

		Logger.info("Going to get the device from the DB for slaveId = " + slaveId);

		Device device = deviceDao.findDeviceBySlaveId(slaveId);

		Logger.info("Got the device from the DB for slaveId = " + slaveId);

		DeviceConf deviceConf = new DeviceConf(
				device.getSlaveId(),
				device.getStatus(),
				device.getSerialPort(),
				device.getIpAddress(),
				device.getIpPort(),
				device.getSlaveLocation(),
				device.getSlaveDescription()
				);

		return deviceConf;

	}


	/**
	 *
	 */
	@Override
	public DeviceEmail getDeviceEmail(byte slaveId) {

		Logger.info("Going to get the device from the DB for slaveId = " + slaveId);

		Device device = deviceDao.findDeviceBySlaveId(slaveId);

		Logger.info("Got the device from the DB for slaveId = " + slaveId);

		DeviceEmail deviceEmail = new DeviceEmail(
				device.getSlaveId(),
				device.getName1(),
				device.getPhone1(),
				device.getEmail1(),
				device.getName2(),
				device.getPhone2(),
				device.getEmail2(),
				device.getName3(),
				device.getPhone3(),
				device.getEmail3(),
				device.getName4(),
				device.getPhone4(),
				device.getEmail4(),
				device.getName5(),
				device.getPhone5(),
				device.getEmail5()
		);

		return deviceEmail;

	}


	/**
	 *
	 */
	@Override
	public AlarmConfSet1 getAlarmConf1(byte slaveId) {

		Logger.info("Going to get the alarm descriptions for set 1 from the DB for slaveId = " + slaveId);

		List<AlarmDescription> alarmDescriptionList = alarmDao.findAllAlarmDescriptions(slaveId);

		Logger.info("Got all the alarm descriptions for set 1 from the DB for slaveId = " + slaveId);

		AlarmConfSet1 alarmConfSet1 = new AlarmConfSet1(
				slaveId,
				alarmDescriptionList.get(0).getAlarmDescription(),
				alarmDescriptionList.get(1).getAlarmDescription(),
				alarmDescriptionList.get(2).getAlarmDescription(),
				alarmDescriptionList.get(3).getAlarmDescription(),
				alarmDescriptionList.get(4).getAlarmDescription(),
				alarmDescriptionList.get(5).getAlarmDescription(),
				alarmDescriptionList.get(6).getAlarmDescription(),
				alarmDescriptionList.get(7).getAlarmDescription(),
				alarmDescriptionList.get(8).getAlarmDescription(),
				alarmDescriptionList.get(9).getAlarmDescription(),
				alarmDescriptionList.get(10).getAlarmDescription(),
				alarmDescriptionList.get(11).getAlarmDescription(),
				alarmDescriptionList.get(12).getAlarmDescription(),
				alarmDescriptionList.get(13).getAlarmDescription(),
				alarmDescriptionList.get(14).getAlarmDescription()
		);

		return alarmConfSet1;

	}


	/**
	 *
	 */
	@Override
	public AlarmConfSet2 getAlarmConf2(byte slaveId) {

		Logger.info("Going to get the alarm descriptions for set 2 from the DB for slaveId = " + slaveId);

		List<AlarmDescription> alarmDescriptionList = alarmDao.findAllAlarmDescriptions(slaveId);

		Logger.info("Got all the alarm descriptions for set 2 from the DB for slaveId = " + slaveId);

		AlarmConfSet2 alarmConfSet2 = new AlarmConfSet2(
				slaveId,
				alarmDescriptionList.get(15).getAlarmDescription(),
				alarmDescriptionList.get(16).getAlarmDescription(),
				alarmDescriptionList.get(17).getAlarmDescription(),
				alarmDescriptionList.get(18).getAlarmDescription(),
				alarmDescriptionList.get(19).getAlarmDescription(),
				alarmDescriptionList.get(20).getAlarmDescription(),
				alarmDescriptionList.get(21).getAlarmDescription(),
				alarmDescriptionList.get(22).getAlarmDescription(),
				alarmDescriptionList.get(23).getAlarmDescription(),
				alarmDescriptionList.get(24).getAlarmDescription(),
				alarmDescriptionList.get(25).getAlarmDescription(),
				alarmDescriptionList.get(26).getAlarmDescription(),
				alarmDescriptionList.get(27).getAlarmDescription(),
				alarmDescriptionList.get(28).getAlarmDescription(),
				alarmDescriptionList.get(29).getAlarmDescription()
		);

		return alarmConfSet2;

	}

	@Override
	public String getSmptFile() {

		String smtpConfData = null;
		try {
			// read smtp.conf from disk
			smtpConfData = new String(Files.readAllBytes(Paths.get("/opt/mac/smtp.conf")));
		} catch (IOException e) {
			e.printStackTrace();
			smtpConfData = "errore durante la lettura del file smtp.conf";
		}
		return smtpConfData;

	}

	@Override
	public void setAdminEmail(String email) {

		userDao.updateAdminEmail(email);

	}


	/**
	 * Getters and Setters
	 */

	public AlarmDao getAlarmDao() {
		return alarmDao;
	}


	public void setAlarmDao(AlarmDao alarmDao) {
		this.alarmDao = alarmDao;
	}


	public CommandDao getCommandDao() {
		return commandDao;
	}


	public void setCommandDao(CommandDao commandDao) {
		this.commandDao = commandDao;
	}


	public DeviceDao getDeviceDao() {
		return deviceDao;
	}


	public void setDeviceDao(DeviceDao deviceDao) {
		this.deviceDao = deviceDao;
	}


	public UserDao getUserDao() {
		return userDao;
	}


	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}
} // end MacService

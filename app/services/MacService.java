/*

Copyright (C) 2008 Giovanni Di Mingo

    This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software 
Foundation; either version 2 of the License, or any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place, 
Suite 330, Boston, MA 02111-1307 USA

Author: Giovanni Di Mingo
Email:  giovanni@dimingo.com

*/

/*
 * IMacService.java
 *
 * Created on July 20, 2008, 8:34 PM
 *
 */

package services;

import java.util.List;

import com.dimingo.poliform.mac.commons.bean.AlarmDescription;

import models.SingleAlarm;
import models.DeviceConf;
import models.DeviceEmail;
import models.AlarmConf;
import models.AlarmConfSet1;
import models.AlarmConfSet2;


/**
 *
 * @author Giovanni Di Mingo
 */
public interface MacService {
    
    List<SingleAlarm> getAlarms(byte slaveId);
    
    AlarmDescription getAlarmDescription(byte slaveId, byte alarmId);
    
    List<AlarmDescription> getAllAlarmDescriptions(byte slaveId);
    
    Boolean getMute(byte slaveId);
    
    List<String> getEmails(byte slaveId);
    
    void resetAlarms(byte slaveId);
    
    void muteAlarms(byte slaveId);

    Boolean getMuteCommand(byte slaveId);

    void updateMute(byte slaveId, Boolean mute);

    void saveDeviceConf(DeviceConf deviceConf);

    void saveDeviceEmail(DeviceEmail deviceEmail);

    void saveAlarmConf(AlarmConf alarmConf);

    void saveSmtpConf(String smtpConf);

    DeviceConf getDeviceConf(byte slaveId);

    DeviceEmail getDeviceEmail(byte slaveId);

    AlarmConfSet1 getAlarmConf1(byte slaveId);

    AlarmConfSet2 getAlarmConf2(byte slaveId);

    String getSmptFile();

    void setAdminEmail(String email);

}

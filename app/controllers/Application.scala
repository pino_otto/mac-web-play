
package controllers

import securesocial.core.{Identity, Authorization, SecureSocial}

import actors._
import services._
import models.DeviceConf
import models.DeviceEmail
import models.AlarmConf
import models.AlarmConfSet1
import models.AlarmConfSet2

import play.api.mvc._
import play.api.mvc.Results._
import play.api.data._
import play.api.data.Forms._
import play.api.Logger
import play.api.Routes
import play.api.libs.json._
import play.api.libs.concurrent._
import play.api.libs.iteratee._
import play.api.Play.current
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.iteratee.{Enumerator, Iteratee}
import play.api.modules.spring.Spring

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.Random
import scala.sys.process._
import scala.language.postfixOps

import akka.actor.Props
import akka.pattern.ask
import akka.util.Timeout

import org.springframework.beans.factory.annotation._
import org.apache.commons.text.StringEscapeUtils.escapeJson


object Application extends Controller with securesocial.core.SecureSocial with Secured {

  lazy val log = Logger("application." + this.getClass.getName)

  val macService = Spring.getBean("macService").asInstanceOf[MacService]

  //  macService.getAlarms(1.asInstanceOf[Byte])

  //  def home = Action { implicit request =>
  //    Ok(views.html.home())
  //  }

  /**
   * action for admin page
   * @return
   */
  def admin = SecuredAction { implicit request =>
    Ok(views.html.admin(request.user))
  }


  /**
   * action for conf page (secured)
   * @return
   */
  def conf = SecuredAction(WithUser("admin")) { implicit request =>
    Ok(views.html.conf(request))
  }


  /**
   * action for smtp page (secured)
   * @return
   */
  def smtp = SecuredAction(WithUser("admin")) { implicit request =>
    Ok(views.html.smtp(request))
  }


  // a sample action using the new authorization hook
  def onlyTwitter = SecuredAction(WithProvider("twitter")) { implicit request =>
    //
    //    Note: If you had a User class and returned an instance of it from UserService, this
    //          is how you would convert Identity to your own class:
    //
    //    request.user match {
    //      case user: User => // do whatever you need with your user class
    //      case _ => // did not get a User instance, should not happen,log error/thow exception
    //    }
    Ok("You can see this because you logged in using Twitter")
  }

  // ----

  /**
   * action for home page
   * @return
   */
  def home = withAuth {
    //    implicit request => userId =>
    userId =>
      implicit request =>
        //      Ok(views.html.home(Security.username))
        //      Ok(views.html.home(SecureSocial.currentUser.getOrElse(None)))
        Ok(views.html.home(request))
  }


  /**
   * action for setup page
   * @return
   */
  def setup(serialNumber: String, email: String) = withAuth {
    //    implicit request => userId =>
    userId =>
      implicit request =>
        //      Ok(views.html.home(Security.username))
        //      Ok(views.html.home(SecureSocial.currentUser.getOrElse(None)))

        var cpuSerialNumber = ""

        try {
          cpuSerialNumber = getCpuSerialNumber
        } catch {
          case e: Exception =>
            log error "exception while getCpuSerialNumber: " + e.getMessage
            cpuSerialNumber = serialNumber + "X"
        }

        log debug "serialNumber = " + serialNumber
        log debug "cpuSerialNumber = " + cpuSerialNumber

        if (serialNumber == cpuSerialNumber) {
          // set the email for admin user
          macService.setAdminEmail(email)
          Ok(views.html.setup(request))
        } else {
          log debug "wrong CPU serial number"
          Unauthorized("wrong CPU serial number")
        }
  }


  /**
   * action for restart mac-web (secured)
   * @return
   */
  def restartMacWeb = SecuredAction(WithUser("admin")) { implicit request =>
    Seq("bash", "-c", "nohup /opt/mac/restart-mac-web.sh &") run
//    "/opt/mac/restart-mac-web.sh" !! // not working?

    Ok(views.html.reboot(request)) //FIXME
  }


  /**
   * action for reboot computer (secured)
   * @return
   */
  def reboot = SecuredAction(WithUser("admin")) { implicit request =>
    Seq("bash", "-c", "sudo reboot") run;
    Ok(views.html.reboot(request))
  }


  /**
   * action for halt computer (secured)
   * @return
   */
  def halt = SecuredAction(WithUser("admin")) { implicit request =>
    Seq("bash", "-c", "sudo halt") run;
    Ok(views.html.halt(request))
  }


  def timer = withAuth {
    implicit request =>
      userId =>
        Ok(views.html.timer())
  }


  // create timer actor
  val timerActor = Akka.system.actorOf(Props[TimerActor])


  /**
    * This function create a WebSocket using the
    * enumerator linked to the current user,
    * retrieved from the TimerActor.
    */
  def indexWS = withAuthWS {

    userId =>

      implicit val timeout = Timeout(3 seconds)

      // using the ask pattern of akka, 
      // get the enumerator for that user
      (timerActor ? StartSocket(userId)) map {
        enumerator =>

          // create a Iteratee which ignore the input and
          // and send a SocketClosed message to the actor when
          // connection is closed from the client
          (Iteratee.ignore[JsValue] map {
            _ =>
              timerActor ! SocketClosed(userId)
          }, enumerator.asInstanceOf[Enumerator[JsValue]])
      }

  } // end indexWS


  def start = withAuth {
    userId =>
      implicit request =>
        timerActor ! Start(userId)
        Ok("")
  }


  def stop = withAuth {
    userId =>
      implicit request =>
        timerActor ! Stop(userId)
        Ok("")
  }


  // create email actor
  val emailActor = Akka.system.actorOf(Props[EmailActor])

  // create alarm detect actor
  val alarmDetectActor = Akka.system.actorOf(Props(classOf[AlarmDetectActor], macService, emailActor))

  //TODO get the list of active slaveId

  // create alarm actors
  val alarmActorList = for (slaveId <- 1 to 1) //TODO use the list of active slaveId
                          yield Akka.system.actorOf(Props(classOf[AlarmActor], slaveId, macService, alarmDetectActor))
//  val alarmActor = Akka.system.actorOf(Props(classOf[AlarmActor], 1, macService, alarmDetectActor))

  // create configuration actor
  val configurationActor = Akka.system.actorOf(Props(classOf[ConfigurationActor], macService, emailActor))


  /**
    * This function create a WebSocket using the
    * enumerator linked to the current user,
    * retrieved from the AlarmActor.
    */
  def indexAlarmWS(slaveId: String) = withAuthWS {

    userId =>

      log debug "************** indexAlarmWS, slaveId = " + slaveId

      implicit val timeout = Timeout(30 seconds)

      // using the ask pattern of akka, 
      // get the enumerator for that user
      (alarmActorList(slaveId.toInt - 1) ? StartAlarmSocket(userId)) map {
        enumerator =>

          // create a Iteratee which ignore the input and
          // and send a SocketClosed message to the actor when
          // connection is closed from the client
          (Iteratee.ignore[JsValue] map {
            _ =>
              alarmActorList(slaveId.toInt - 1) ! AlarmSocketClosed(userId)
          }, enumerator.asInstanceOf[Enumerator[JsValue]])
      }

  } // end indexAlarmWS


  def resetAlarms(slaveId: String) = SecuredAction(WithUser("admin")) { implicit request =>

    log debug "************** resetAlarms"
    alarmActorList(slaveId.toInt - 1) ! ResetAlarms()
    Ok("")

  } // end resetAlarms


  //  def muteAlarms = SecuredAction(WithUser("admin")) { implicit request =>
  def muteAlarms(slaveId: String) = Action { implicit request =>

    log debug "************** muteAlarms"
    alarmActorList(slaveId.toInt - 1) ! MuteAlarms()
    Ok("")

  } // end muteAlarms


  implicit val ByteRead = new Reads[Byte] {
    override def reads(json: JsValue): JsResult[Byte] = JsSuccess(json.as[Int].toByte)
  }

  implicit val ByteWrite = new Writes[Byte] {
    override def writes(o: Byte): JsNumber  = JsNumber(o)
  }

  implicit val DeviceConfWrites = Json.writes[DeviceConf]
  implicit val DeviceEmailWrites = Json.writes[DeviceEmail]
  implicit val AlarmConfSet1Writes = Json.writes[AlarmConfSet1]
  implicit val AlarmConfSet2Writes = Json.writes[AlarmConfSet2]


  def getDeviceConf(slaveId: String) = SecuredAction(WithUser("admin")) { implicit request =>

    log debug "************** getDeviceConf"
    log debug "slaveId = " + slaveId

    val deviceConf = macService.getDeviceConf(slaveId.toByte)

    val deviceConfAsJson = Json.toJson(deviceConf)

    log debug "deviceConfAsJson = " + deviceConfAsJson

    Ok(deviceConfAsJson)

  } // end getDeviceConf


  def getDeviceEmail(slaveId: String) = SecuredAction(WithUser("admin")) { implicit request =>

    log debug "************** getDeviceEmail"
    log debug "slaveId = " + slaveId

    val deviceEmail = macService.getDeviceEmail(slaveId.toByte)

    val deviceEmailAsJson = Json.toJson(deviceEmail)

    log debug "deviceEmailAsJson = " + deviceEmailAsJson

    Ok(deviceEmailAsJson)

  } // end getDeviceEmail


  def getAlarmConf1(slaveId: String) = SecuredAction(WithUser("admin")) { implicit request =>

    log debug "************** getAlarmConf1"
    log debug "slaveId = " + slaveId

    val alarmConfSet1 = macService.getAlarmConf1(slaveId.toByte)

    val alarmConfSetAsJson1 = Json.toJson(alarmConfSet1)

    log debug "alarmConfSetAsJson = " + alarmConfSetAsJson1

    Ok(alarmConfSetAsJson1)

  } // end getAlarmConf1


  def getAlarmConf2(slaveId: String) = SecuredAction(WithUser("admin")) { implicit request =>

    log debug "************** getAlarmConf2"
    log debug "slaveId = " + slaveId

    val alarmConfSet2 = macService.getAlarmConf2(slaveId.toByte)

    val alarmConfSetAsJson2 = Json.toJson(alarmConfSet2)

    log debug "alarmConfSetAsJson = " + alarmConfSetAsJson2

    Ok(alarmConfSetAsJson2)

  } // end getAlarmConf2


  /*
  val deviceForm = Form(
	  mapping(
	    //"slaveId" -> number,
	    //"status" -> number,
	    "serialPort" -> text,
	    "ipAddress" -> text,
	    "ipPort" -> text,
	    "slaveLocation" -> text,
	    "slaveDescription" -> text
	  )(DeviceConf.apply)(DeviceConf.unapply)
  )
  */

  val deviceConfForm = Form(
    tuple(
      "slaveId" -> number,
      "status" -> text,
      "serialPort" -> text,
      "ipAddress" -> text,
      "ipPort" -> text,
      "slaveLocation" -> text,
      "slaveDescription" -> text
    )
  )

  val deviceEmailForm = Form(
    tuple(
      "slaveId" -> number,
      "name_1" -> text,
      "phone_1" -> text,
      "email_1" -> text,
      "name_2" -> text,
      "phone_2" -> text,
      "email_2" -> text,
      "name_3" -> text,
      "phone_3" -> text,
      "email_3" -> text,
      "name_4" -> text,
      "phone_4" -> text,
      "email_4" -> text,
      "name_5" -> text,
      "phone_5" -> text,
      "email_5" -> text
    )
  )

  val alarmConfForm1 = Form(
    tuple(
      "slaveId" -> number,
//      "slaveId" -> ignored(1),
      "alarmDescription_01" -> text,
      "alarmDescription_02" -> text,
      "alarmDescription_03" -> text,
      "alarmDescription_04" -> text,
      "alarmDescription_05" -> text,
      "alarmDescription_06" -> text,
      "alarmDescription_07" -> text,
      "alarmDescription_08" -> text,
      "alarmDescription_09" -> text,
      "alarmDescription_10" -> text,
      "alarmDescription_11" -> text,
      "alarmDescription_12" -> text,
      "alarmDescription_13" -> text,
      "alarmDescription_14" -> text,
      "alarmDescription_15" -> text
    )
  )

  val alarmConfForm2 = Form(
    tuple(
      "slaveId" -> number,
      "alarmDescription_16" -> text,
      "alarmDescription_17" -> text,
      "alarmDescription_18" -> text,
      "alarmDescription_19" -> text,
      "alarmDescription_20" -> text,
      "alarmDescription_21" -> text,
      "alarmDescription_22" -> text,
      "alarmDescription_23" -> text,
      "alarmDescription_24" -> text,
      "alarmDescription_25" -> text,
      "alarmDescription_26" -> text,
      "alarmDescription_27" -> text,
      "alarmDescription_28" -> text,
      "alarmDescription_29" -> text,
      "alarmDescription_30" -> text
    )
  )

  val smtpConfForm = Form(
    single(
      "data" -> text
    )
  )


  def getSmtpConf = SecuredAction(WithUser("admin")) { implicit request =>

    log debug "************** getSmtpConf"

    val smtpConf = macService.getSmptFile

    log debug "smtpConf = " + smtpConf

    val smtpConfAsJson = "{\"data\": \"" + escapeJson(smtpConf) + "\"}"

    log debug "smtpConfAsJson = " + smtpConfAsJson

    Ok(smtpConfAsJson)

  } // end getSmtpConf


  def updateDeviceConf = SecuredAction(WithUser("admin")) { implicit request =>

    log debug "************** updateDeviceConf"

    log debug "request = " + request

    // get the data from the request
    val (
      slaveId,
      status,
      serialPort,
      ipAddress,
      ipPort,
      slaveLocation,
      slaveDescription
      ) = deviceConfForm.bindFromRequest.get

    log debug "slaveId = " + slaveId
    log debug "status = " + status
    log debug "serialPort = " + serialPort
    log debug "ipAddress = " + ipAddress
    log debug "ipPort = " + ipPort
    log debug "slaveLocation = " + slaveLocation
    log debug "slaveDescription = " + slaveDescription

    val deviceConf = DeviceConf(
      slaveId.toByte,
      status,
      serialPort,
      ipAddress,
      ipPort,
      slaveLocation,
      slaveDescription
    )

    log debug "deviceConf = " + deviceConf

    configurationActor ! deviceConf
    Ok("")

  } // end updateDeviceConf


  def updateDeviceEmail = SecuredAction(WithUser("admin")) { implicit request =>

    log debug "************** updateDeviceEmail"

    log debug "request = " + request

    // get the data from the request
    val (
      slaveId,
      name_1,
      phone_1,
      email_1,
      name_2,
      phone_2,
      email_2,
      name_3,
      phone_3,
      email_3,
      name_4,
      phone_4,
      email_4,
      name_5,
      phone_5,
      email_5
      ) = deviceEmailForm.bindFromRequest.get

    log debug "slaveId = " + slaveId
    log debug "name_1 = " + name_1
    log debug "phone_1 = " + phone_1
    log debug "email_1 = " + email_1
    log debug "name_2 = " + name_2
    log debug "phone_2 = " + phone_2
    log debug "email_2 = " + email_2
    log debug "name_3 = " + name_3
    log debug "phone_3 = " + phone_3
    log debug "email_3 = " + email_3
    log debug "name_4 = " + name_4
    log debug "phone_4 = " + phone_4
    log debug "email_4 = " + email_4
    log debug "name_5 = " + name_5
    log debug "phone_5 = " + phone_5
    log debug "email_5 = " + email_5

    val deviceEmail = DeviceEmail(
      slaveId.toByte,
      name_1,
      phone_1,
      email_1,
      name_2,
      phone_2,
      email_2,
      name_3,
      phone_3,
      email_3,
      name_4,
      phone_4,
      email_4,
      name_5,
      phone_5,
      email_5
    )

    log debug "deviceEmail = " + deviceEmail

    configurationActor ! deviceEmail
    Ok("")

  } // end updateDeviceEmail


  def updateAlarmConf1 = SecuredAction(WithUser("admin")) { implicit request =>

    log debug "************** updateAlarmConf1"

    log debug "request = " + request

    // get the data from the request
    val (
      slaveId,
      alarmDescription_01,
      alarmDescription_02,
      alarmDescription_03,
      alarmDescription_04,
      alarmDescription_05,
      alarmDescription_06,
      alarmDescription_07,
      alarmDescription_08,
      alarmDescription_09,
      alarmDescription_10,
      alarmDescription_11,
      alarmDescription_12,
      alarmDescription_13,
      alarmDescription_14,
      alarmDescription_15
      ) = alarmConfForm1.bindFromRequest.get

    log debug "slaveId = " + slaveId
    log debug "alarmDescription_01 = " + alarmDescription_01
    log debug "alarmDescription_02 = " + alarmDescription_02
    log debug "alarmDescription_03 = " + alarmDescription_03
    log debug "alarmDescription_04 = " + alarmDescription_04
    log debug "alarmDescription_05 = " + alarmDescription_05
    log debug "alarmDescription_06 = " + alarmDescription_06
    log debug "alarmDescription_07 = " + alarmDescription_07
    log debug "alarmDescription_08 = " + alarmDescription_08
    log debug "alarmDescription_09 = " + alarmDescription_09
    log debug "alarmDescription_10 = " + alarmDescription_10
    log debug "alarmDescription_11 = " + alarmDescription_11
    log debug "alarmDescription_12 = " + alarmDescription_12
    log debug "alarmDescription_13 = " + alarmDescription_13
    log debug "alarmDescription_14 = " + alarmDescription_14
    log debug "alarmDescription_15 = " + alarmDescription_15

    configurationActor ! AlarmConf(slaveId.toByte, 1.toByte, alarmDescription_01)
    configurationActor ! AlarmConf(slaveId.toByte, 2.toByte, alarmDescription_02)
    configurationActor ! AlarmConf(slaveId.toByte, 3.toByte, alarmDescription_03)
    configurationActor ! AlarmConf(slaveId.toByte, 4.toByte, alarmDescription_04)
    configurationActor ! AlarmConf(slaveId.toByte, 5.toByte, alarmDescription_05)
    configurationActor ! AlarmConf(slaveId.toByte, 6.toByte, alarmDescription_06)
    configurationActor ! AlarmConf(slaveId.toByte, 7.toByte, alarmDescription_07)
    configurationActor ! AlarmConf(slaveId.toByte, 8.toByte, alarmDescription_08)
    configurationActor ! AlarmConf(slaveId.toByte, 9.toByte, alarmDescription_09)
    configurationActor ! AlarmConf(slaveId.toByte, 10.toByte, alarmDescription_10)
    configurationActor ! AlarmConf(slaveId.toByte, 11.toByte, alarmDescription_11)
    configurationActor ! AlarmConf(slaveId.toByte, 12.toByte, alarmDescription_12)
    configurationActor ! AlarmConf(slaveId.toByte, 13.toByte, alarmDescription_13)
    configurationActor ! AlarmConf(slaveId.toByte, 14.toByte, alarmDescription_14)
    configurationActor ! AlarmConf(slaveId.toByte, 15.toByte, alarmDescription_15)

    Ok("")

  } // end updateAlarmConf1

  def updateAlarmConf2 = SecuredAction(WithUser("admin")) { implicit request =>

    log debug "************** updateAlarmConf2"

    log debug "request = " + request

    // get the data from the request
    val (
      slaveId,
      alarmDescription_16,
      alarmDescription_17,
      alarmDescription_18,
      alarmDescription_19,
      alarmDescription_20,
      alarmDescription_21,
      alarmDescription_22,
      alarmDescription_23,
      alarmDescription_24,
      alarmDescription_25,
      alarmDescription_26,
      alarmDescription_27,
      alarmDescription_28,
      alarmDescription_29,
      alarmDescription_30
      ) = alarmConfForm2.bindFromRequest.get

    log debug "slaveId = " + slaveId
    log debug "alarmDescription_16 = " + alarmDescription_16
    log debug "alarmDescription_17 = " + alarmDescription_17
    log debug "alarmDescription_18 = " + alarmDescription_18
    log debug "alarmDescription_19 = " + alarmDescription_19
    log debug "alarmDescription_20 = " + alarmDescription_20
    log debug "alarmDescription_21 = " + alarmDescription_21
    log debug "alarmDescription_22 = " + alarmDescription_22
    log debug "alarmDescription_23 = " + alarmDescription_23
    log debug "alarmDescription_24 = " + alarmDescription_24
    log debug "alarmDescription_25 = " + alarmDescription_25
    log debug "alarmDescription_26 = " + alarmDescription_26
    log debug "alarmDescription_27 = " + alarmDescription_27
    log debug "alarmDescription_28 = " + alarmDescription_28
    log debug "alarmDescription_29 = " + alarmDescription_29
    log debug "alarmDescription_30 = " + alarmDescription_30

    configurationActor ! AlarmConf(slaveId.toByte, 16.toByte, alarmDescription_16)
    configurationActor ! AlarmConf(slaveId.toByte, 17.toByte, alarmDescription_17)
    configurationActor ! AlarmConf(slaveId.toByte, 18.toByte, alarmDescription_18)
    configurationActor ! AlarmConf(slaveId.toByte, 19.toByte, alarmDescription_19)
    configurationActor ! AlarmConf(slaveId.toByte, 20.toByte, alarmDescription_20)
    configurationActor ! AlarmConf(slaveId.toByte, 21.toByte, alarmDescription_21)
    configurationActor ! AlarmConf(slaveId.toByte, 22.toByte, alarmDescription_22)
    configurationActor ! AlarmConf(slaveId.toByte, 23.toByte, alarmDescription_23)
    configurationActor ! AlarmConf(slaveId.toByte, 24.toByte, alarmDescription_24)
    configurationActor ! AlarmConf(slaveId.toByte, 25.toByte, alarmDescription_25)
    configurationActor ! AlarmConf(slaveId.toByte, 26.toByte, alarmDescription_26)
    configurationActor ! AlarmConf(slaveId.toByte, 27.toByte, alarmDescription_27)
    configurationActor ! AlarmConf(slaveId.toByte, 28.toByte, alarmDescription_28)
    configurationActor ! AlarmConf(slaveId.toByte, 29.toByte, alarmDescription_29)
    configurationActor ! AlarmConf(slaveId.toByte, 30.toByte, alarmDescription_30)

    Ok("")

  } // end updateAlarmConf2


  def updateSmtpConf = SecuredAction(WithUser("admin")) { implicit request =>

    log debug "************** updateSmtpConf"

    log debug "request = " + request

    // get the data from the request
    val smtpConfData = smtpConfForm.bindFromRequest.get

    log debug "smtpConfData = " + smtpConfData

    macService.saveSmtpConf(smtpConfData)

    Ok("")

  } // end updateSmtpConf


  def getCpuSerialNumber: String = {

    // SN=$(cat /proc/cpuinfo | grep Serial | cut -d ' ' -f 2)
//    val cpuSerialNumber = Seq("bash", "-c", "echo 'Serial : 0000000008066b68' | grep Serial | cut -d ' ' -f 3") !!  // testing
    val cpuSerialNumber = Seq("bash", "-c", "cat /proc/cpuinfo | grep Serial | cut -d ' ' -f 2") !!  // production

//    log debug "CPU serial number = " + cpuSerialNumber
//    log debug "CPU serial number size = " + cpuSerialNumber.length
//
//    log debug "CPU serial number - 1= " + cpuSerialNumber.substring(0, cpuSerialNumber.length - 1)
//    log debug "CPU serial number size -1  = " + (cpuSerialNumber.length - 1)

    cpuSerialNumber.trim.replaceAll("\\n", "")

  } // end getCpuSerialNumber


  def javascriptRoutes = Action {
    implicit request =>
      Ok(
        Routes.javascriptRouter("jsRoutes")(
          routes.javascript.Application.indexWS,
          routes.javascript.Application.start,
          routes.javascript.Application.stop,
          routes.javascript.Application.indexAlarmWS,
          routes.javascript.Application.resetAlarms,
          routes.javascript.Application.muteAlarms,
          routes.javascript.Application.updateDeviceConf,
          routes.javascript.Application.updateDeviceEmail,
          routes.javascript.Application.updateAlarmConf1,
          routes.javascript.Application.updateAlarmConf2,
          routes.javascript.Application.updateSmtpConf,
          routes.javascript.Application.getDeviceConf,
          routes.javascript.Application.getDeviceEmail,
          routes.javascript.Application.getAlarmConf1,
          routes.javascript.Application.getAlarmConf2,
          routes.javascript.Application.getSmtpConf,
          routes.javascript.Application.restartMacWeb,
          routes.javascript.Application.reboot,
          routes.javascript.Application.halt
        )
      ).as("text/javascript")
  }

} // end Application


// An Authorization implementation that only authorizes users that logged in using twitter
case class WithProvider(provider: String) extends Authorization {
  def isAuthorized(user: Identity) = {
    user.identityId.providerId == provider
  }
}


// An Authorization implementation that only authorizes users that logged in as admin user
case class WithUser(username: String) extends Authorization {
  def isAuthorized(user: Identity) = {
    user.identityId.userId == username
  }
}


trait Secured {

  def username(request: RequestHeader) = {
    //verify or create session, this should be a real login
    request.session.get(Security.username)
  }

  /**
    * When user not have a session, this function create a
    * random userId and reload index page
    */
  def unauthF(request: RequestHeader) = {
    val newId: String = new Random().nextInt().toString()
    Redirect(routes.Application.home).withSession(Security.username -> newId)
  }

  /**
    * Basic authentication system
    * try to retrieve the username, call f() if it is present,
    * or unauthF() otherwise
    */
  def withAuth(f: => Int => Request[_ >: AnyContent] => Result): EssentialAction = {
    Security.Authenticated(username, unauthF) {
      username =>
        Action(request => f(username.toInt)(request))
    }
  }

  /**
    * This function provide a basic authentication for
    * WebSocket, likely withAuth function try to retrieve the
    * the username from the session, and call f() function if find it,
    * or create an error Future[(Iteratee[JsValue, Unit], Enumerator[JsValue])])
    * if username is none
    */
  def withAuthWS(f: => Int => Future[(Iteratee[JsValue, Unit], Enumerator[JsValue])]): WebSocket[JsValue] = {

    // this function create an error Future[(Iteratee[JsValue, Unit], Enumerator[JsValue])])
    // the iteratee ignore the input and do nothing,
    // and the enumerator just send a 'not authorized message'
    // and close the socket, sending Enumerator.eof
    def errorFuture = {
      // Just consume and ignore the input
      val in = Iteratee.ignore[JsValue]

      // Send a single 'not authorized' message and close
      val out = Enumerator(Json.toJson("not authorized")).andThen(Enumerator.eof)

      Future {
        (in, out)
      }
    }

    WebSocket.async[JsValue] {
      request =>
        username(request) match {
          case None =>
            errorFuture

          case Some(id) =>
            f(id.toInt)

        }
    }

  }

}

package actors

import akka.actor.Actor
import play.api.Logger

import scala.concurrent.duration._
import scala.sys.process._
import scala.language.postfixOps



class SoundActor extends Actor {

  import context._

  lazy val log = Logger("application." + this.getClass.getName)

  val playCommand = "ogg123"

  val soundFilename = "/opt/mac/sounds/siren-noise.ogg"

  var play: Boolean = false


  /**
    *
    */
  override def receive = {

    case StartPlay =>

      log.debug(s"sound actor received a message to start to play sound")
      play = true
      self ! PlaySound


    case StopPlay =>

      log.debug(s"sound actor received a message to stop to play sound")
      play = false


    case PlaySound =>

      // stop previous sound (if any)
      ("pkill -f " + soundFilename) !

      if (play) {
        log.debug(s"playing... $soundFilename")
        (playCommand + " " + soundFilename) !
      }
//      system.scheduler.scheduleOnce(200 millis, self, PlaySound)


    case _ =>

      log.error("unhandled message")

      unhandled()

  } // end receive

} // end SoundActor


sealed trait SoundMessage

case class StartPlay() extends SoundMessage

case class StopPlay() extends SoundMessage

case class PlaySound() extends SoundMessage
package actors

import sys.process._
import scala.concurrent.duration._
import scala.util.Random
import scala.collection.JavaConverters._
import scala.collection.JavaConversions._
import scala.collection.immutable.List
import scala.collection.mutable.Map
import play.api.libs.json._
import play.api.libs.json.Json._
import play.api.libs.iteratee.{Concurrent, Enumerator}
import play.api.libs.iteratee.Concurrent.Channel
import play.api.Logger
import play.api.libs.concurrent.Execution.Implicits._
import akka.actor.{Actor, ActorRef, Props}
import models.SingleAlarm
import models.AlarmPackage
import models.Email
import play.api.libs.concurrent.Akka
import services.MacService

class AlarmDetectActor(macService: MacService, emailActor: ActorRef) extends Actor {

  lazy val log = Logger("application." + this.getClass.getName)

  val playCommand = "ogg123"

  val soundFilename = "/opt/mac/ramdisk/sounds/alarm-sound.ogg"

  var canPlaySound = true

  var previousMute = true

  var newMute = true

  var soundCounter = 0

  val MAX_SOUND_COUNTER = 5

//  // create sound actor
//  val soundActor = context.actorOf(Props[SoundActor])

  //  // get email actor
  //	ActorRef emailActor = getContext().actorFor(
  //			"akka://converterKernel@" + settings.CONVERTER_HOSTNAME + ":" +
  //					settings.CONVERTER_PORT + "/user/emailActor");

  // initialize the previous alarms map (slaveId -> alarm list)
  val previousAlarmListMap: Map[Int, List[SingleAlarm]] =
    Map(
      1 -> List(),
      2 -> List(),
      3 -> List(),
      4 -> List(),
      5 -> List(),
      6 -> List(),
      7 -> List(),
      8 -> List(),
      9 -> List(),
      10 -> List(),
      11 -> List(),
      12 -> List(),
      13 -> List(),
      14 -> List(),
      15 -> List(),
      16 -> List(),
      17 -> List(),
      18 -> List(),
      19 -> List(),
      20 -> List()
    )


  /**
    *
    */
  override def receive = {

    case AlarmPackage(slaveId, alarmList) =>

      log.debug(s"analyze alarms for slave $slaveId, alarmList: $alarmList")

      val previousAlarmList = previousAlarmListMap.get(slaveId).get

      log.debug(s"previousAlarmList: $previousAlarmList")

      // check for empty list
      if (previousAlarmList.isEmpty) { // just the first time for initialization

        // save the new list as previous list
        //  		    previousAlarmListMap.updated(slaveId, alarmList)
        previousAlarmListMap.put(slaveId, alarmList)

      } else { // normal condition

        // get the sublist of gray
        val grayAlarmList = alarmList.filter(_.getStatus().equals("gray"))

        log.debug(s"grayAlarmList: $grayAlarmList")

        // check for all alarms are gray, that means "reset"
        if (grayAlarmList.size == 30) {
          canPlaySound = true
          soundCounter = 0
        }

        // get the sublist of red
        val redAlarmList = alarmList.filter(_.getStatus().equals("red"))

        log.debug(s"redAlarmList: $redAlarmList")

        // check for not empty red alarm list
        if (!redAlarmList.isEmpty) {

          // get the mute from modbus device (0 = no sound, 1 = sound)
          newMute = macService.getMute(slaveId.toByte)

          if (previousMute && !newMute) {
            // set the mute command in DB to 1
            macService.updateMute(slaveId.toByte, true)
          }

          // get the mute command from DB (0 = sound, 1 = no sound)
          val muteCommand = macService.getMuteCommand(slaveId.toByte)

          if (muteCommand && canPlaySound) {
            canPlaySound = false;
          }

          previousMute = newMute;

          log.debug(s"slave $slaveId canPlaySound: $canPlaySound")
          log.debug("0 = no sound, 1 = sound")

          log.debug(s"soundCounter = $soundCounter")

          if (canPlaySound && soundCounter == 0) {
            // play alarm sound on the server (raspberry pi)
            log.debug(s"playing... $soundFilename")
//            (playCommand + " " + soundFilename) ! // synchronous
            (playCommand + " " + soundFilename) run // asynchronous
          }

          soundCounter += 1
          if (soundCounter >= MAX_SOUND_COUNTER) {
            soundCounter = 0
          }

          var emailText = ""

          // compare the current red alarm with the previous alarm
          redAlarmList.foreach(singleAlarm =>

            if ((previousAlarmList.find(_.getId() == singleAlarm.getId()).get.getStatus().equals("gray") ||
              previousAlarmList.find(_.getId() == singleAlarm.getId()).get.getStatus().equals("green")) &&
              singleAlarm.getStatus().equals("red")) {

              emailText += "Allarme " + singleAlarm.getId() + ": " + singleAlarm.getDescription() + "\n"

              log.debug(s"emailText: $emailText")

            } // end if

          ) // end foreach

          // get the list of email recipients for this slave
          val emailList = macService.getEmails(slaveId.asInstanceOf[Byte]).toList

          log.debug(s"emailList: $emailList")

          // check if there is new alarm
          if (!emailText.isEmpty()) {

            // get the email sender
            val emailSender = play.Play.application.configuration.getString("smtp.from")

            log.debug(s"emailSender: $emailSender")

            // for each email send message to email actor
            emailList.foreach(emailAddress => {

              // create the email
              val email = Email(emailSender,
                emailAddress,
                "Allarmi",
                emailText
              )

              // send email to email actor
              emailActor ! email

              log.debug(s"alarm email sent to email actor for slave $slaveId, email address $emailAddress")

            }) // end foreach

          } // end if

        } else { // no red alarm

          // get the mute command from DB (0 = sound, 1 = no sound)
          val muteCommand = macService.getMuteCommand(slaveId.toByte)

          if (muteCommand) {
            // reset mute command in DB (mute = 0)
            macService.updateMute(slaveId.toByte, false)
          }

          soundCounter = 0

        }

        // save the new list as previous list
        previousAlarmListMap.put(slaveId, alarmList)

      } // end else normal condition

    case _ =>

      log.error("unhandled message")

      unhandled()

  } // end receive


} // end AlarmDetectActor
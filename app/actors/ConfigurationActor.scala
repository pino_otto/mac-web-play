package actors


import akka.actor.Actor
import akka.actor.ActorRef

import play.api.libs.json._
import play.api.libs.json.Json._
import play.api.libs.iteratee.{Concurrent, Enumerator}
import play.api.libs.iteratee.Concurrent.Channel
import play.api.Logger
import play.api.libs.concurrent.Execution.Implicits._

import scala.concurrent.duration._
import scala.util.Random
import scala.collection.JavaConverters._
import scala.collection.JavaConversions._
import scala.collection.immutable.List
import scala.collection.mutable.Map

import services.MacService
import models.SingleAlarm
import models.AlarmPackage
import models.Email
import models.DeviceConf
import models.DeviceEmail
import models.AlarmConf


class ConfigurationActor(macService: MacService, emailActor: ActorRef) extends Actor {

  lazy val log = Logger("application." + this.getClass.getName)

  //  // get email actor
  //	ActorRef emailActor = getContext().actorFor(
  //			"akka://converterKernel@" + settings.CONVERTER_HOSTNAME + ":" +
  //					settings.CONVERTER_PORT + "/user/emailActor");


  /**
    *
    */
  override def receive = {

//    case deviceConf: DeviceConf(slaveId, status, serialPort, ipAddress, ipPort, slaveLocation, slaveDescription) =>
    case deviceConf: DeviceConf =>

      log.debug(s"received DeviceConf: $deviceConf")
//      log.debug(s"received DeviceConf for slave $(deviceConf.slaveId)")

      log debug "slaveId = " + deviceConf.slaveId
      log debug "status = " + deviceConf.status
      log debug "serialPort = " + deviceConf.serialPort
      log debug "ipAddress = " + deviceConf.ipAddress
      log debug "ipPort = " + deviceConf.ipPort
      log debug "slaveLocation = " + deviceConf.slaveLocation
      log debug "slaveDescription = " + deviceConf.slaveDescription

      macService.saveDeviceConf(deviceConf)

    case deviceEmail: DeviceEmail =>

      log.debug(s"received DeviceEmail: $deviceEmail")
//      log.debug(s"received DeviceEmail for slave $(deviceEmail.slaveId)")

      log debug "name_1 = " + deviceEmail.name_1
      log debug "phone_1 = " + deviceEmail.phone_1
      log debug "email_1 = " + deviceEmail.email_1
      log debug "name_2 = " + deviceEmail.name_2
      log debug "phone_2 = " + deviceEmail.phone_2
      log debug "email_2 = " + deviceEmail.email_2
      log debug "name_3 = " + deviceEmail.name_3
      log debug "phone_3 = " + deviceEmail.phone_3
      log debug "email_3 = " + deviceEmail.email_3
      log debug "name_4 = " + deviceEmail.name_4
      log debug "phone_4 = " + deviceEmail.phone_4
      log debug "email_4 = " + deviceEmail.email_4
      log debug "name_5 = " + deviceEmail.name_5
      log debug "phone_5 = " + deviceEmail.phone_5
      log debug "email_5 = " + deviceEmail.email_5

      macService.saveDeviceEmail(deviceEmail)

    case alarmConf: AlarmConf =>

      log.debug(s"received AlarmConf: $alarmConf")

      log debug "slaveId = " + alarmConf.slaveId
      log debug "alarmId = " + alarmConf.alarmId
      log debug "alarmDescription = " + alarmConf.alarmDescription

      macService.saveAlarmConf(alarmConf)

    case _ =>

      log.error("unhandled message")

      unhandled()

  } // end receive


} // end ConfigurationActor
package actors;

import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import models.Email;
import misc.Emailer;

public class EmailActor extends UntypedActor {

    LoggingAdapter log = Logging.getLogger(getContext().system(), this);
    //TODO see how to change the logger

    private String emailSender = "info@openalarm.com";
    private String emailRecipient = "xxx@gmail.com";
    private String emailSubject = "Allarme";

    private Emailer emailer = null;

    /**
     *
     */
    @Override
    public void preStart() {

        try {

            super.preStart();

        } catch (Exception e) {

            e.printStackTrace();
            log.error(e.getMessage());

        } // end catch

        // create emailer
        emailer = new Emailer();

        log.info("Started EmailActor");
        log.info("default emailSender: {}", emailSender);
        log.info("default emailRecipient: {}", emailRecipient);
        log.info("default emailSubject: {}", emailSubject);

    } // end preStart


    /**
     *
     */
    public void onReceive(Object message) {

        if (message instanceof Email) { // email object (all fields)

            // get the email object
            Email email = (Email) message;
            log.info("Received email object: {}", email);

            String to = email.to();

            if (to != null && !to.isEmpty()) {

                // send email
                emailer.send(email.from(), to, email.subject(), email.text());

            }

        } else if (message instanceof String) { // string message (only email text)

            // get email text
            String emailText = (String) message;
            log.info("Received email text: {}", emailText);

            // send email
            emailer.send(emailSender, emailRecipient, emailSubject, emailText);

        } else { // not handled message

            unhandled(message);

        } // end else

    } // end onReceive


    /**
     * Setters
     */
    public void setEmailSender(String emailSender) {
        this.emailSender = emailSender;
    }

    public void setEmailRecipient(String emailRecipient) {
        this.emailRecipient = emailRecipient;
    }

    public void setEmailSubject(String emailSubject) {
        this.emailSubject = emailSubject;
    }

} // end EmailActor

package actors

import play.api.libs.json._
import play.api.libs.json.Json._

import akka.actor.Actor
import akka.actor.ActorRef

import play.api.libs.iteratee.{Concurrent, Enumerator}

import play.api.libs.iteratee.Concurrent.Channel
import play.api.Logger
import play.api.libs.concurrent.Execution.Implicits._

import scala.concurrent.duration._
import scala.util.Random
import scala.collection.JavaConverters._
import scala.collection.JavaConversions._

import services.MacService
import models.SingleAlarm
import models.AlarmPackage


/**
  *
  */
class AlarmActor(slaveId: Int, macService: MacService, alarmDetectActor: ActorRef) extends Actor {

  log debug s"slaveId = $slaveId"

  val periodInSeconds = 1

  var soundCounter = 0

  val MAX_SOUND_COUNTER = 5

  //  macService.getAlarms(slaveId.asInstanceOf[Byte])

  // create a scheduler to send a message to this actor every N seconds
  val cancellable = context.system.scheduler.schedule(0 second, periodInSeconds second, self, UpdateAlarms())

  case class UserChannel(userId: Int, var channelsCount: Int, enumerator: Enumerator[JsValue], channel: Channel[JsValue])

  lazy val log = Logger("application." + this.getClass.getName)

  // this map relate every user with his UserChannel
  var webSockets = Map[Int, UserChannel]()

  /**
    *
    */
  override def receive = {

    case StartAlarmSocket(userId) =>

      log.debug(s"start new alarm socket for user $userId")

      // get or create the tuple (Enumerator[JsValue], Channel[JsValue]) for current user
      // Channel is very useful class, it allows to write data inside its related 
      // enumerator, that allow to create WebSocket or Streams around that enumerator and
      // write data inside that using its related Channel
      val userChannel: UserChannel = webSockets.get(userId) getOrElse {
        val broadcast: (Enumerator[JsValue], Channel[JsValue]) = Concurrent.broadcast[JsValue]
        UserChannel(userId, 0, broadcast._1, broadcast._2)
      }

      // if user open more than one connection, increment just a counter instead of create
      // another tuple (Enumerator, Channel), and return current enumerator,
      // in that way when we write in the channel,
      // all opened WebSocket of that user receive the same data
      userChannel.channelsCount = userChannel.channelsCount + 1
      webSockets += (userId -> userChannel)

      log debug s"channel for user : $userId count : ${userChannel.channelsCount}"
      log debug s"channel count : ${webSockets.size}"

      // return the enumerator related to the user channel,
      // this will be used for create the WebSocket
      sender ! userChannel.enumerator

    case UpdateAlarms() =>

      // get the alarms from the database
      //      val alarmList = macService.getAlarms(1.asInstanceOf[Byte]) //FIXME slaveId
      val alarmList = macService.getAlarms(slaveId.asInstanceOf[Byte])

      log debug alarmList.toString

      val alarms = alarmList.asScala

      // split the alarm list into 2 lists
      val (alarms1, alarms2) = alarms splitAt 15

//      // get the mute from the database (modbus device) <--- old logic
//      //      val mute = macService.getMute(1.asInstanceOf[Byte]) //FIXME slaveId
//      val mute = macService.getMute(slaveId.asInstanceOf[Byte])

      // get the mute from the database (command) <-- new logic
      val mute = macService.getMuteCommand(slaveId.asInstanceOf[Byte])

      //      implicit val alarmFormat = (Json.format[SingleAlarm])(SingleAlarm.apply, unlift(SingleAlarm.unapply))

      implicit val alarmWrites = new Writes[SingleAlarm] {
        def writes(s: SingleAlarm): JsValue = {
          Json.obj(
            "id" -> s.getId(),
            "description" -> s.getDescription(),
            "status" -> s.getStatus(),
            "image" -> s.getImageFilename()
          )
        }
      }

      //      val alarms = new Random().nextInt
      //      val sound = new Random().nextInt(2)
      //      val sound = if (mute) 0 else 1

      // analyze the alarms, if there is alarm, send email and set sound; use email actor for no-blocking email processing

      // create an alarm package
      val alarmPackage = AlarmPackage(slaveId, alarms.toList)

      // send alarm package to alarm detect actor
      alarmDetectActor ! alarmPackage

      // remove the sound
      var sound = 0

      // get the sublist of red alarms
      val redAlarmList = alarms.filter(_.getStatus().equals("red"))

      // check for not empty red alarm list
      if (!redAlarmList.isEmpty) { // red alarm

        log debug "soundCounter = " + soundCounter

        // set the sound
        //        sound = if (mute) 0 else 1
        if (mute) {
          sound = 0
        } else if (soundCounter == 0) {
          sound = 1
        } else {
          sound = 0
        }

        soundCounter += 1
        if (soundCounter >= MAX_SOUND_COUNTER) {
          soundCounter = 0
        }

      } else { // no red alarm
        soundCounter = 0
      }

      //      val jsonAlarms = Json.obj("alarmData" -> alarmList) //  Map("alarmData" -> toJson(alarms))
      //      val jsonAlarmSound = Map("alarmSound" -> toJson(sound))

      val dataMap = Map("alarmData1" -> toJson(alarms1), "alarmData2" -> toJson(alarms2), "alarmSound" -> toJson(sound))
      log debug "dataMap = " + dataMap

      val dataMapJson = Json.toJson(dataMap)
      log debug "dataMapJson = " + dataMapJson

      // writing data to the channel,
      // will send data to all WebSocket opened from every user
      webSockets.foreach {
        case (userId, userChannel) =>
          //        	webSockets.get(userId).get.channel push Json.toJson(json)
          //        	userChannel.channel push Json.toJson(jsonAlarms)
          //        	userChannel.channel push Json.toJson(jsonAlarmSound)
          log debug "start to push json to websocket, userId = " + userId
          userChannel.channel push dataMapJson
          log debug "finished to push json to websocket, userId = " + userId
      }

      log debug "finished to process UpdateAlarms"

    case ResetAlarms() =>

      log debug "+++++++++++++ received ResetAlarms"

      // write the reset field in the db
      macService.resetAlarms(slaveId.asInstanceOf[Byte])


    case MuteAlarms() =>

      log debug "+++++++++++++ received MuteAlarms"

      // write the mute field in the db
      macService.muteAlarms(slaveId.asInstanceOf[Byte])


    case AlarmSocketClosed(userId) =>

      log debug s"closed alarm socket for $userId"

      val userChannel = webSockets.get(userId).get

      if (userChannel.channelsCount > 1) {
        userChannel.channelsCount = userChannel.channelsCount - 1
        webSockets += (userId -> userChannel)
        log debug s"channel for user : $userId count : ${userChannel.channelsCount}"
      } else {
        removeUserChannel(userId)
        log debug s"removed channel for $userId"
      }

  }

  def removeUserChannel(userId: Int) = webSockets -= userId

} // end AlarmActor


sealed trait AlarmSocketMessage

case class StartAlarmSocket(userId: Int) extends AlarmSocketMessage

case class AlarmSocketClosed(userId: Int) extends AlarmSocketMessage

case class UpdateAlarms() extends AlarmSocketMessage

case class ResetAlarms() extends AlarmSocketMessage

case class MuteAlarms() extends AlarmSocketMessage



package misc

import com.typesafe.plugin._
import play.api.Play.current


class Emailer {

  /**
   * Send a single email.
   */
  def send(from: String, to: String, subject: String, body: String): Unit = {

    val mail = use[MailerPlugin].email
    mail.setSubject(subject)
    mail.setRecipient(to)
    mail.setFrom(from)

    mail.send(body)

  }

}

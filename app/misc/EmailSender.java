package misc;

/*
   Configuration file:
  
	  	# Configuration file for javax.mail 
		# If a value for an item is not provided, then 
		# system defaults will be used. These items can 
		# also be set in code.
		
		# Host whose mail services will be used 
		# (Default value : localhost) 
		mail.host=mail.blah.com
		
		# Return address to appear on emails 
		# (Default value : username@host) 
		mail.from=webmaster@blah.net
		
		# Other possible items include: 
		# mail.user= 
		# mail.store.protocol= 
		# mail.transport.protocol= 
		# mail.smtp.host= 
		# mail.smtp.user= 
		# mail.debug=
 */

	
import java.util.*;
import java.io.*;

import javax.mail.*;
import javax.mail.internet.*;

import play.Logger;

/**
* Class to send email.
*
* Run from the command line. Please edit the implementation
* to use correct email addresses and host name.
*/
public class EmailSender {
	
	private Properties fMailServerConfig = new Properties();
	
	
	/**
	 * Constructor
	 */
	public EmailSender() {
		
		fetchConfig();
		
	} // end constructor
	
	
	/**
	 * Constructor
	 * 
	 * @param configFilename full path of configuration file
	 */
	public EmailSender(String configFilename) {
		
		fetchConfig(configFilename);
		
	} // end constructor


	/**
	 * Send a single email.
	 * 
	 * @param aFromEmailAddr
	 * @param aToEmailAddr
	 * @param aSubject
	 * @param aBody
	 */
	public void sendEmail(
			String aFromEmailAddr, 
			String aToEmailAddr,
			String aSubject, 
			String aBody) {
		
		// Here, no Authenticator argument is used (it is null).
		// Authenticators are used to prompt the user for user
		// name and password.
		
		// open a session
		Session session = Session.getDefaultInstance(fMailServerConfig, null);
		
		// create a message
		MimeMessage message = new MimeMessage(session);
		
		try {
			
			// the "from" address may be set in code, or set in the
			// config file under "mail.from"
			message.setFrom(new InternetAddress(aFromEmailAddr));
			
			// set the recipient
			message.addRecipient(Message.RecipientType.TO, 
					new InternetAddress(aToEmailAddr));
			
			// set the subject
			message.setSubject(aSubject);
			
			// set the body
			message.setText(aBody);
			
			// send the message
			Transport.send(message);
			
			Logger.info("Email sent successfully.");
			
		} catch (MessagingException ex) {
			
			Logger.error("Cannot send email. " + ex);
			
		} // end catch
		
	} // end sendEmail
	
	
	/**
	 * Send a single email.
	 * 
	 * The from parameters is read from configuration file
	 */
	public void sendEmail(String aToEmailAddr, String aSubject, String aBody) {
		
		// Here, no Authenticator argument is used (it is null).
		// Authenticators are used to prompt the user for user
		// name and password.
		
		// open a session
		Session session = Session.getDefaultInstance(fMailServerConfig, null);
		
		// create a message
		MimeMessage message = new MimeMessage(session);
		
		try {
			
			// set the recipient
			message.addRecipient(Message.RecipientType.TO, 
					new InternetAddress(aToEmailAddr));
			
			// set the subject
			message.setSubject(aSubject);
			
			// set the body
			message.setText(aBody);
			
			// send the message
			Transport.send(message);
			
			Logger.info("Email sent successfully.");
			
		} catch (MessagingException ex) {
			
			Logger.error("Cannot send email. " + ex);
			
		} // end catch
		
	} // end sendEmail
	

	/**
	 * Allows the config to be refreshed at runtime, instead of requiring a
	 * restart.
	 */
	public void refreshConfig() {
		
		fMailServerConfig.clear();
		fetchConfig();
		
	} // end refreshConfig
	
	
	/**
	 * Allows the config to be refreshed at runtime, instead of requiring a
	 * restart.
	 * 
	 * @param configFilename full path of configuration file
	 */
	public void refreshConfig(String configFilename) {
		
		fMailServerConfig.clear();
		fetchConfig(configFilename);
		
	} // end refreshConfig

	
	// -------------------------------------------------------------------------------
	// PRIVATE //
	
	/**
	 * Open a specific text file containing mail server parameters, and populate
	 * a corresponding Properties object.
	 * 
	 * @param configFilename full path of configuration file
	 */
	private void fetchConfig() {
		
		fetchConfig("/opt/mac/mail.conf");
		
	} // end fetchConfig

	/**
	 * Open a specific text file containing mail server parameters, and populate
	 * a corresponding Properties object.
	 * 
	 * @param configFilename full path of configuration file
	 */
	private void fetchConfig(String configFilename) {
		
		InputStream input = null;
		
		try {
			
			// If possible, one should try to avoid hard-coding a path in this
			// manner; in a web application, one should place such a file in
			// WEB-INF, and access it using ServletContext.getResourceAsStream.
			// Another alternative is Class.getResourceAsStream.
			// This file contains the javax.mail config properties mentioned
			// above.
			
			// open the config file
			input = new FileInputStream(configFilename);
			
			// read the configuration
			fMailServerConfig.load(input);
			
		} catch (IOException ex) {
			
			Logger.error("Cannot open and load mail server properties file.");
			
		} finally {
			
			try {
				
				if (input != null) input.close();
				
			} catch (IOException ex) {
				
				Logger.error("Cannot close mail server properties file.");
				
			} // end catch
			
		} // end finally
		
	} // end fetchConfig
	
	
	/**
	 * 
	 * @param aArguments
	 */
	public static void main(String... aArguments) {

		// create email sender
		EmailSender emailer = new EmailSender();
		
		// the domains of these email addresses should be valid,
		// or the example will fail:
		emailer.sendEmail("info@dimingo.com", "pino.otto@gmail.com",
				"Testing 1-2-3", "blah blah blah");
		
	} // end main
	
	
} // end EmailSender


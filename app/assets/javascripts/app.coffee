
utils = window.angular.module('utils' , ['ngCookies'])

utils.filter('timer' ,['time', (time) ->
  (input) ->
    if input
      seconds=toSeconds(input)
      minutes=toMinutes(input)
      hours=toHours(input)
      "#{hours}:#{minutes}:#{seconds}"
    else
      "Press Start"
]).service('time', ->
    @toHours = (timeMillis) -> addZero((timeMillis/(1000*60*60)))
    @toMinutes = (timeMillis) -> addZero((timeMillis/(1000*60))%60)
    @toSeconds = (timeMillis) -> addZero((timeMillis/1000)%60)
    @toTime = (hours,minutes,seconds) -> ((hours * 60 * 60) + (minutes * 60) + seconds) * 1000
    @addZero = (value) ->
      value = Math.floor(value)
      if(value < 10)
        "0#{value}"
      else
        value

).controller('TimerController', ($scope, $http) ->

  startWS = ->
    wsUrl = jsRoutes.controllers.Application.indexWS().webSocketURL()
  
    $scope.socket = new WebSocket(wsUrl)
    $scope.socket.onmessage = (msg) ->
      $scope.$apply( ->
        console.log "received : #{msg}"
        $scope.time = JSON.parse(msg.data).data      
      )

  $scope.start = ->
    $http.get(jsRoutes.controllers.Application.start().url).success( -> )

  $scope.stop = ->
    $http.get(jsRoutes.controllers.Application.stop().url).success( -> )

  startWS()

).controller('AlarmController', ($scope, $http, $interval, $cookieStore, $cookies, audio) ->

  startAlarmWS = ->
    wsUrl = jsRoutes.controllers.Application.indexAlarmWS(1).webSocketURL()
    console.log "wsUrl : #{wsUrl}"

    $scope.msgCount = 0
    $scope.oldMsgCount = -1;

    checkMsgCount = ->
        if ($scope.msgCount == $scope.oldMsgCount)
            console.log "no new messages received for a while"
            $scope.socket.close()
            location.reload(true)
        else
            $scope.oldMsgCount = $scope.msgCount
            console.log "all ok, new messages received"

    $interval(checkMsgCount, 20000); # time in ms

    #define websocket
    $scope.socket = new WebSocket(wsUrl)

    $scope.socket.onopen = ->
        console.log "websocket open"

    $scope.socket.onclose = ->
        console.log "websocket close"
        location.reload(true)

    $scope.socket.onerror = ->
        console.log "websocket error"

    $scope.socket.onmessage = (msg) ->
      $scope.$apply( ->
        $scope.msgCount = $scope.msgCount + 1
        console.log "received : #{msg}" + " msgCount =  " + $scope.msgCount
        $scope.alarmData1 = JSON.parse(msg.data).alarmData1
        $scope.alarmData2 = JSON.parse(msg.data).alarmData2
        $scope.alarmSound = JSON.parse(msg.data).alarmSound     
        
        if ($scope.alarmSound == 1) then audio.play("/assets/sounds/sound_1.ogg")
        if ($scope.alarmSound == 0) then audio.pause()

#         if ($scope.msgCount % 2 == 0 ) then $scope.progress = "o"
#         if ($scope.msgCount % 2 == 1 ) then $scope.progress = " "

        if ($scope.msgCount % 2 == 0 ) then $scope.progress = "blue-circle-1.png"
        if ($scope.msgCount % 2 == 1 ) then $scope.progress = "blue-circle-2.png"

      )
      
  $scope.resetAlarms = ->
    $http.get(jsRoutes.controllers.Application.resetAlarms(1).url).success( -> )

  $scope.muteAlarms = ->
    $http.get(jsRoutes.controllers.Application.muteAlarms(1).url).success( -> )

  startAlarmWS()

).controller('ConfController', ($scope, $http) ->

# handle device conf

  $scope.deviceConf = {};

  $scope.deviceConf.slaveId = 1;

  $http.get(jsRoutes.controllers.Application.getDeviceConf($scope.deviceConf.slaveId).url).success( (data) ->
    $scope.deviceConf = data;
    ).error (data) ->
       alert 'failed'
       return

  $scope.updateDeviceConf = ->
    $http.post(jsRoutes.controllers.Application.updateDeviceConf().url, $scope.deviceConf).success( -> )

# handle device email

  $scope.deviceEmail = {};

  $scope.deviceEmail.slaveId = 1;

  $http.get(jsRoutes.controllers.Application.getDeviceEmail($scope.deviceEmail.slaveId).url).success( (data) ->
    $scope.deviceEmail = data;
    ).error (data) ->
       alert 'failed'
       return

  $scope.updateDeviceEmail = ->
    $http.post(jsRoutes.controllers.Application.updateDeviceEmail().url, $scope.deviceEmail).success( -> )

# handle alarm conf

  $scope.alarmConf1 = {};

  $scope.alarmConf1.slaveId = 1;

  $http.get(jsRoutes.controllers.Application.getAlarmConf1($scope.alarmConf1.slaveId).url).success( (data) ->
    $scope.alarmConf1 = data;
    ).error (data) ->
      alert 'failed'
      return

  $scope.updateAlarmConf1 = ->
    $http.post(jsRoutes.controllers.Application.updateAlarmConf1().url, $scope.alarmConf1).success( -> )

  $scope.alarmConf2 = {};

  $scope.alarmConf2.slaveId = 1;

  $http.get(jsRoutes.controllers.Application.getAlarmConf2($scope.alarmConf2.slaveId).url).success( (data) ->
    $scope.alarmConf2 = data;
    ).error (data) ->
      alert 'failed'
      return

  $scope.updateAlarmConf2 = ->
    $http.post(jsRoutes.controllers.Application.updateAlarmConf2().url, $scope.alarmConf2).success( -> )

).controller('SmtpController', ($scope, $http) ->

# handle smtp conf

  $scope.smtpConf = {};

  $http.get(jsRoutes.controllers.Application.getSmtpConf().url).success( (data) ->
    $scope.smtpConf = data;
    console.log "smtpConf = " + $scope.smtpConf.data
    ).error (data) ->
       alert 'failed'
       return

  $scope.updateSmtpConf = ->
    $http.post(jsRoutes.controllers.Application.updateSmtpConf().url, $scope.smtpConf).success( -> )

)



# embedded js code
`
utils.factory('audio', function ($document) {
  var audioElement = $document[0].createElement('audio');
  return {
    audioElement: audioElement,

    play: function(filename) {
        audioElement.src = filename;
        audioElement.play();   
    },
    
    pause: function() {
        audioElement.pause();   
    }

  }
});
`

###
myModule = window.angular.module('myModule' , [])

myModule.factory('audio', (document) ->
  audioElement = document[0].createElement('audio')
  
    audioElement: audioElement

    play: (filename) ->
        audioElement.src = filename;
        audioElement.load;
        audioElement.play();
  
)
###



window.angular.module('app' , ['utils', 'ui.bootstrap'])

addZero = (value) ->
  value = Math.floor(value)
  if(value < 10)
    "0#{value}"
  else
    value

toHours = (time) ->
  addZero((time/(1000*60*60)))
toMinutes = (time) ->
  addZero((time/(1000*60))%60)
toSeconds = (time) ->
  addZero((time/1000)%60)
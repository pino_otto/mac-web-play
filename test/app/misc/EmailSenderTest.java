package misc;

import org.junit.*;

import play.mvc.*;
import play.test.*;
import play.libs.F.*;

import misc.EmailSender;

import static play.test.Helpers.*;
import static org.fest.assertions.Assertions.*;

public class EmailSenderTest {

    @Test
    public void simpleCheck() {
        int a = 1 + 1;
        assertThat(a).isEqualTo(2);
    }

    @Test
    public void sendEmail() {
        EmailSender emailSender = new EmailSender();
        // the domains of these email addresses should be valid, or the example will fail
        emailSender.sendEmail("info@dimingo.com",
                "pino.otto@gmail.com",
                "Testing 1-2-3",
                "blah blah blah"
        );
    }



}

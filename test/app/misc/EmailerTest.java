package misc;

import org.junit.*;

import play.mvc.*;
import play.test.*;
import play.libs.F.*;

import misc.Emailer;

import play.api.test.FakeApplication;

import static play.test.Helpers.*;
import static org.fest.assertions.Assertions.*;

public class EmailerTest {

    @Test
    public void simpleCheck() {
        int a = 1 + 1;
        assertThat(a).isEqualTo(2);
    }

    @Test
    public void sendEmail() {
        Emailer emailer = new Emailer();
        // the domains of these email addresses should be valid, or the example will fail
        emailer.send("info@dimingo.com",
                "pino.otto@gmail.com",
                "Testing 1-2-3",
                "blah blah blah"
        );
    }



}

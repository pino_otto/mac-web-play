package test

import actors.{SoundActor, StartPlay, StopPlay}
import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestActorRef, TestKit}
import org.scalatest.matchers.ShouldMatchers
import org.scalatest.{BeforeAndAfterAll, FlatSpec}
import play.api.test.FakeApplication


class SoundActorTest()
  extends TestKit(ActorSystem("MySpec"))
    with ImplicitSender with ShouldMatchers with FlatSpec with BeforeAndAfterAll {

  play.api.Play.start(FakeApplication())

  val soundActorRef = TestActorRef[SoundActor]

  "A Sound Actor" should
    "be able to play sound" in {
      soundActorRef ! StartPlay

      soundActorRef ! StopPlay

      Thread.sleep(10000)
      soundActorRef ! StopPlay

      println("stopped")

      Thread.sleep(10000)
      println("exit")
    }

}

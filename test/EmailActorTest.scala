package test

import actors.{EmailActor}
import models.Email
import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestActorRef, TestKit}
import org.scalatest.matchers.ShouldMatchers
import org.scalatest.{BeforeAndAfterAll, FlatSpec}
import play.api.test.FakeApplication


class EmailActorTest()
  extends TestKit(ActorSystem("MySpec"))
    with ImplicitSender with ShouldMatchers with FlatSpec with BeforeAndAfterAll {

  play.api.Play.start(FakeApplication())

  val emailActorRef = TestActorRef[EmailActor]

  val email = Email("info@dimingo.com", "pino.otto@gmail.com", "test", "EmailActorTest")

  "An Email Actor" should
    "be able to send email" in {
      emailActorRef ! email
      println("email sent")
    }

}
